package com.example.sipanji;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;


import androidx.fragment.app.Fragment;

import com.example.sipanji.ui.notifikasi.NotifikasiFragment;
import com.example.sipanji.ui.riwayat.RiwayatFragment;
import com.example.sipanji.util.SharedPrefManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Notification extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().hasExtra("msg")) {
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Notifikasi")
                .setContentText(getIntent().getExtras().getString("msg"))
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        Intent intent = new Intent(Notification.this, MainActivity.class);
                        intent.putExtra("redirect", "goto-notifikasi");
                        startActivity(intent);

                        sDialog.dismissWithAnimation();
                        finish();
                    }
                })
                .show();
        }

    }
}
