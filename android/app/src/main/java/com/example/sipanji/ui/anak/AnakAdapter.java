package com.example.sipanji.ui.anak;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sipanji.R;
import com.example.sipanji.ui.menu.MenuAnakFragment;
import com.example.sipanji.ui.profile.ProfileFragment;
import com.example.sipanji.util.api.UtilsApi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class AnakAdapter extends RecyclerView.Adapter<AnakAdapter.AnakViewHolder> {

    private ArrayList<AnakModel> dataList;

    public AnakAdapter(ArrayList<AnakModel> dataList) {
        this.dataList = dataList;
    }


    @NonNull
    @Override
    public AnakViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_anak_list, parent, false);
        return new AnakAdapter.AnakViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnakViewHolder holder, int position) {

        String nama = dataList.get(position).getNama();
        String tgl_lahir = dataList.get(position).getTanggal_lahir();
        int usia = dataList.get(position).getUsia();

        holder.tvNamalengkap.setText(String.format(Locale.US, "%s", nama));
        holder.tvTanggalLahir.setText(String.format(Locale.US,"%s ( %d Bulan )",tgl_lahir,usia));

        Picasso.get().invalidate(UtilsApi.BASE_URL + "data_file/" +  dataList.get(position).getFoto());
        Picasso.get().load(UtilsApi  .BASE_URL + "data_file/" +  dataList.get(position).getFoto()).into(holder.foto);



        holder.btnProfile.setOnClickListener(view -> {
            Log.i("OnClick", String.valueOf(dataList.get(position).getId()));
            Bundle bundle = new Bundle();
            bundle.putInt("pasien_id", dataList.get(position).getId());
            bundle.putString("pasien_jk", dataList.get(position).getJk());
            bundle.putString("pasien_tgl_lahir", dataList.get(position).getTanggal_lahir());
            bundle.putInt("pasien_usia", dataList.get(position).getUsia());
            bundle.putString("pasien_nama",dataList.get(position).getNama());
            bundle.putString("pasien_tempat_lahir",dataList.get(position).getTempat_lahir());
            bundle.putString("pasien_foto",dataList.get(position).getFoto());

            ProfileFragment fragment = new ProfileFragment();
            fragment.setArguments(bundle);
            AppCompatActivity activity = (AppCompatActivity) view.getContext();

            activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, ProfileFragment.class.getSimpleName())
                .addToBackStack(null)
                .commit();
        });
        holder.btnMenu.setOnClickListener(view -> {

            Log.i("OnClick", String.valueOf(dataList.get(position).getId()));
            Bundle bundle = new Bundle();
            bundle.putInt("pasien_id", dataList.get(position).getId());
            bundle.putString("pasien_jk", dataList.get(position).getJk());
            bundle.putString("pasien_tgl_lahir", dataList.get(position).getTanggal_lahir());
            bundle.putInt("pasien_usia", dataList.get(position).getUsia());
            bundle.putString("pasien_nama",dataList.get(position).getNama());
            bundle.putString("pasien_tempat_lahir",dataList.get(position).getTempat_lahir());

            MenuAnakFragment fragment = new MenuAnakFragment();
            fragment.setArguments(bundle);
            AppCompatActivity activity = (AppCompatActivity) view.getContext();

            activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, MenuAnakFragment.class.getSimpleName())
                .addToBackStack(null)
                .commit();
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class AnakViewHolder extends RecyclerView.ViewHolder {
        TextView tvNamalengkap;
        TextView tvTanggalLahir;
        Button btnProfile;
        Button btnMenu;
        ImageView foto;

        public AnakViewHolder(View itemView) {
            super(itemView);
            tvNamalengkap = itemView.findViewById(R.id.tvAnak_nama);
            tvTanggalLahir = itemView.findViewById(R.id.tvAnak_tgllahir);
            btnProfile = itemView.findViewById(R.id.anak_btnProfile);
            btnMenu = itemView.findViewById(R.id.anak_btnMenu);
            foto = itemView.findViewById(R.id.imgViewAnak_foto);
        }
    }
}
