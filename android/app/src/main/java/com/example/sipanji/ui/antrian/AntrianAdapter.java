package com.example.sipanji.ui.antrian;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sipanji.R;

import java.util.List;
import java.util.Locale;

public class AntrianAdapter extends RecyclerView.Adapter<AntrianAdapter.AntrianViewHolder> {
    OnBindCallBack onBindCallBack;
    private List<AntrianModel> dataList;

    public AntrianAdapter(List<AntrianModel> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public AntrianAdapter.AntrianViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_antrian_list, parent, false);
        return new AntrianAdapter.AntrianViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AntrianAdapter.AntrianViewHolder holder, int position) {
        holder.rowId = dataList.get(position).getId();
        holder.tvTglAntrian.setText(String.format(Locale.US, "Tanggal Antrian : %s", dataList.get(position).getTgl()));
        holder.tvJenis.setText(String.format(Locale.US, "Antrian Untuk : %s", dataList.get(position).getJenis()));
        holder.btnBatal.setOnClickListener(v -> {
            if (onBindCallBack != null) {
                onBindCallBack.OnViewBind("btnBatalOnClick", holder, position);
            }
        });

        holder.btnDetail.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putInt("antrian_id", dataList.get(position).getId());

            AntrianDetail fragment = new AntrianDetail();
            fragment.setArguments(bundle);

            AppCompatActivity activity = (AppCompatActivity) view.getContext();

            activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, AntrianDetail.class.getSimpleName())
                .addToBackStack(null)
                .commit();
        });


        holder.itemView.setOnClickListener(view -> {
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class AntrianViewHolder extends RecyclerView.ViewHolder {
        int rowId;
        TextView tvTglAntrian;
        TextView tvJenis;
        Button btnBatal;
        Button btnDetail;

        public AntrianViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTglAntrian = itemView.findViewById(R.id.recycleAntrian_tvTanggal);
            tvJenis = itemView.findViewById(R.id.recyclerAntrian_tvJenis);

            btnBatal = itemView.findViewById(R.id.recyclerAntrian_btnBatal);
            btnDetail = itemView.findViewById(R.id.recyclerAntrian_btnDetail);
        }
    }
}
