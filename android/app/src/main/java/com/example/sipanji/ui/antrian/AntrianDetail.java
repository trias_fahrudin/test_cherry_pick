package com.example.sipanji.ui.antrian;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.util.api.BaseApiService;
import com.example.sipanji.util.api.UtilsApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AntrianDetail extends Fragment {
    ProgressDialog loading;
    BaseApiService mBaseApiService;
    @BindView(R.id.antrianDetail_barcode)
    ImageView imgBarcode;
    @BindView(R.id.antrianDetail_tvUrutan)
    TextView tvUrutan;
    @BindView(R.id.antrianDetail_tvUrutanSekarang)
    TextView tvSekarang;
    private SwipeRefreshLayout swipe;
    private Context context;
    private int antrian_id;
    private Handler h;
    private Boolean stop = false;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_antrian_detail, container, false);
        swipe = root.findViewById(R.id.antrianDetail_swipeContainer);

        ButterKnife.bind(this, root);

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            antrian_id = getArguments().getInt("antrian_id", 0);
        }

        mBaseApiService = UtilsApi.getAPIService();

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Data Antrian");
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();

            floatingActionButton.setOnClickListener(view -> {

            });
        }


        swipe.setOnRefreshListener(() -> {
            swipe.setRefreshing(false);
            loadData();
        });

        loadData();

        h = new Handler();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!stop) {
                    try {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                AntrianDetail.this.loadData();
                            }
                        });
                        TimeUnit.SECONDS.sleep(60);
                    } catch (Exception ex) {
                    }
                }
            }
        }).start();


// to start the handler


        return root;
    }


    public String padLeftZeros(String inputString, int length) {
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append('0');
        }
        sb.append(inputString);

        return sb.toString();
    }

    private void loadData() {

//        loading = ProgressDialog.show(context, null, "Mengambil data ...", true, false);
        mBaseApiService.getAntrianDetail(this.antrian_id)
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
//                        loading.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getString("error").equals("false")) {

                                String barcode_file = jsonObject.getString("barcode_file");

                                Picasso.get().invalidate(barcode_file);
                                Picasso.get().load(barcode_file).into(imgBarcode);

                                tvUrutan.setText(padLeftZeros(jsonObject.getString("urutan_ke"),2));
                                tvSekarang.setText(padLeftZeros(jsonObject.getString("pasien_diperiksa"),2));

                            } else {
                                String error_message = jsonObject.getString("error_msg");
                                Toasty.error(context, error_message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toasty.error(context, "Ada kesalahan!\n" + t.toString(), Toast.LENGTH_LONG, true).show();
//                    loading.dismiss();
                }
            });

    }


    @Override
    //Pressed return button
    public void onResume() {
        super.onResume();
        requireView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {
            this.stop = true;
            return false;
        });
    }

}
