package com.example.sipanji.ui.antrian;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.util.api.BaseApiService;
import com.example.sipanji.util.api.UtilsApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AntrianFragment extends Fragment {

    ProgressDialog loading;
    BaseApiService mBaseApiService;
    private AntrianAdapter antrianAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipe;
    private Context context;
    private int pasien_id;
    private String pasien_tgl_lahir;
    private int pasien_usia;
    private String pasien_nama;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_antrian, container, false);
        swipe = root.findViewById(R.id.antrian_swipeContainer);

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            pasien_id = getArguments().getInt("pasien_id", 0);
            pasien_tgl_lahir = getArguments().getString("pasien_tgl_lahir", "");
            pasien_usia = getArguments().getInt("pasien_usia", 0);
            pasien_nama = getArguments().getString("pasien_nama", "");
        }

        mBaseApiService = UtilsApi.getAPIService();

        androidx.appcompat.widget.Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Data Antrian untuk " + pasien_nama);
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.show();

            floatingActionButton.setOnClickListener(view -> {

                Bundle bundle = new Bundle();
                bundle.putInt("pasien_id", this.pasien_id);
                bundle.putString("pasien_tgl_lahir", this.pasien_tgl_lahir);
                bundle.putInt("pasien_usia", this.pasien_usia);
                bundle.putString("pasien_nama", this.pasien_nama);

                AntrianPendaftaranForm fragment = new AntrianPendaftaranForm();
                fragment.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();

                activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment, AntrianPendaftaranForm.class.getSimpleName())
                    .addToBackStack(null)
                    .commit();
            });
        }

        recyclerView = root.findViewById(R.id.antrian_recyclerList);

        swipe.setOnRefreshListener(() -> {
            swipe.setRefreshing(false);
            loadData();
        });

        loadData();

        return root;
    }

    private void loadData() {

        loading = ProgressDialog.show(context, null, "Mengambil data ...", true, false);
        mBaseApiService.getAntrian(this.pasien_id)
            .enqueue(new Callback<AntrianModelList>() {
                @Override
                public void onResponse(@NotNull Call<AntrianModelList> call, @NotNull Response<AntrianModelList> response) {
                    if (response.isSuccessful()) {
                        loading.dismiss();
                        generateRecyler(Objects.requireNonNull(response.body()).getArrayList());

                    } else {
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<AntrianModelList> call, Throwable t) {
                    Toasty.error(context, "Ada kesalahan!\n" + t.toString(), Toast.LENGTH_LONG, true).show();
                    loading.dismiss();
                }
            });
    }

    private void generateRecyler(ArrayList<AntrianModel> arrayList) {
        antrianAdapter = new AntrianAdapter(arrayList);

        antrianAdapter.onBindCallBack = (jenis, viewHolder, position) -> {

            if ("btnBatalOnClick".equals(jenis)) {

                new AlertDialog.Builder(this.context)
                    .setTitle("Batalkan antrian")
                    .setMessage("Apakah anda yakin ingin membatalkan antrian ini?")
                    .setPositiveButton("YA !", (dialog, which) -> {
                        loading = ProgressDialog.show(context, null, "Mengambil data ...", true, false);
                        mBaseApiService.postBatalkanAntrian(viewHolder.rowId)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {
                                        loading.dismiss();

                                        swipe.setRefreshing(false);
                                        loadData();

                                    } else {
                                        loading.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Toasty.error(context, "Ada kesalahan!\n" + t.toString(), Toast.LENGTH_LONG, true).show();
                                    loading.dismiss();
                                }
                            });
                    }).setNegativeButton("Tidak", null).show();
            }

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        };


        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(antrianAdapter);
    }
}
