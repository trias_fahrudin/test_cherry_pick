package com.example.sipanji.ui.imunisasi_detail;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ImunisasiDetailFragment extends Fragment {

    private Context mContext;
    private SwipeRefreshLayout swipe;
    private String list_id;
    private String list_nama;
    private String list_dilakukan;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_detail_imunisasi, container, false);
        swipe = root.findViewById(R.id.detail_imunisasi_swipeContainer);
        recyclerView = root.findViewById(R.id.recycler_view_detailimunisasi_list);

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            /*
            *  bundle.putString("list_id", viewHolder.list_id); //3,1,2
               bundle.putString("list_nama", viewHolder.list_nama);//BCG,Hepatitis B-1,Polio-0
               bundle.putString("list_dilakukan", viewHolder.list_dilakukan);//3::2020-07-10,1::2020-07-07,2::2020-07-07

            *
            * */
            list_id = getArguments().getString("list_id", "");
            list_nama = getArguments().getString("list_nama", "");
            list_dilakukan = getArguments().getString("list_dilakukan", "");
        }


        androidx.appcompat.widget.Toolbar toolbar = requireActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Detail Imunisasi");
        FloatingActionButton floatingActionButton = ((MainActivity) requireActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();

            floatingActionButton.setOnClickListener(view -> {

            });
        }

        swipe.setOnRefreshListener(() -> {
            swipe.setRefreshing(false);
            loadData();
        });

        loadData();
        return root;
    }

    private String cekImunisasiDilakukan(String i) {
        String[] arrListDilakukan = this.list_dilakukan.split(",");

        String result = "-";
        for (String arr : arrListDilakukan) {
            String[] parts = arr.split("::");

            for (String part : parts) {
                if (i.equals(part)) {
                    result = parts[1];
                    break;
                }
            }
        }

        return result;

    }

    private void loadData() {

        List<ImunisasiDetailModel> imunisasiDetailModelList = new ArrayList<>();

        String[] arrListId = this.list_id.split(",");
        StringTokenizer tokenizer = new StringTokenizer(this.list_nama, ",");

        int index = 0;
        while (tokenizer.hasMoreTokens()) {
            String cek = this.cekImunisasiDilakukan(arrListId[index]);
            if (!cek.equals("-")) {

                imunisasiDetailModelList.add(new ImunisasiDetailModel(String.format("Dilakukan pada %s", cek), String.format("Imunisasi %s", tokenizer.nextToken())));
            } else {
                imunisasiDetailModelList.add(new ImunisasiDetailModel("(Belum dilakukan imunisasi)", String.format("Imunisasi %s", tokenizer.nextToken())));
            }

            index++;
        }

//        for (int i = 0; i < 100; i++) {
//            imunisasiDetailModelList.add(new ImunisasiDetailModel("tanggal", "nama"));
//        }

        generateList(imunisasiDetailModelList);

    }

    private void generateList(List<ImunisasiDetailModel> imunisasiDetailModelList) {
        ImunisasiDetailAdapter imunisasiDetailAdapter = new ImunisasiDetailAdapter(imunisasiDetailModelList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(imunisasiDetailAdapter);

    }

}
