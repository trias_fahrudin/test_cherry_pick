package com.example.sipanji.ui.imunisasi_detail;

public class ImunisasiDetailModel {

    private String tanggal;
    private String nama_imunisasi;

    public ImunisasiDetailModel(String tanggal, String nama_imunisasi) {
        this.tanggal = tanggal;
        this.nama_imunisasi = nama_imunisasi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNama_imunisasi() {
        return nama_imunisasi;
    }

    public void setNama_imunisasi(String nama_imunisasi) {
        this.nama_imunisasi = nama_imunisasi;
    }

}
