package com.example.sipanji.ui.jadwal_imunisasi;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sipanji.R;

import java.util.ArrayList;
import java.util.Locale;

public class JadwalImunisasiAdapter extends RecyclerView.Adapter<JadwalImunisasiAdapter.JadwalImunisasiViewHolder> {
    OnBindCallBack onBindCallBack;
    private ArrayList<JadwalImunisasiModel> dataList;
    private int usia;

    JadwalImunisasiAdapter(ArrayList<JadwalImunisasiModel> dataList, int usia) {
        this.dataList = dataList;
        this.usia = usia;
    }

    @NonNull
    @Override
    public JadwalImunisasiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_jadwal_imunisasi_list, parent, false);
        return new JadwalImunisasiAdapter.JadwalImunisasiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JadwalImunisasiViewHolder holder, int position) {
//        if (dataList.get(position).getJml_belum_dilakukan() > 0) {
//
//            if (dataList.get(position).getUsia() < this.usia) {
//                holder.llImunisasi_container.setBackgroundColor(Color.parseColor("#721c24"));
//            } else {
//                holder.llImunisasi_container.setBackgroundColor(Color.parseColor("#856404"));
//            }
//
//        } else {
//            holder.llImunisasi_container.setBackgroundColor(Color.parseColor("#155724"));
//        }

        if (dataList.get(position).getJml_sudah_dilakukan() == 0) {
            holder.btnImunisasi_detail.setEnabled(false);
            holder.btnImunisasi_detail.setBackgroundColor(Color.parseColor("#d6d8d9"));
        } else {
            holder.btnImunisasi_detail.setEnabled(true);
            holder.btnImunisasi_detail.setBackgroundColor(Color.parseColor("#536DFE"));
        }

        holder.tvImunisasi_Usia.setText(String.format(Locale.US, "Usia pemberian imunisasi : %d bulan", dataList.get(position).getUsia()));
        holder.tvImunisasi_namaImunisasi.setText(dataList.get(position).getList_nama());
        holder.tvImunisasi_range_usia.setText(String.format(Locale.US, "Tanggal pemberian dianjurkan : %s", dataList.get(position).getRange_usia()));
        holder.tvImunisasi_status.setText(String.format(Locale.US, "Status pemberian : %s", dataList.get(position).getList_status()));
        holder.list_id = dataList.get(position).getList_id();
        holder.list_nama = dataList.get(position).getList_nama();
        holder.list_dilakukan = dataList.get(position).getList_dilakukan();

        holder.btnImunisasi_detail.setOnClickListener(v -> {
            if (onBindCallBack != null) {
                onBindCallBack.OnViewBind("detail", holder, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class JadwalImunisasiViewHolder extends RecyclerView.ViewHolder {

        LinearLayout llImunisasi_container;
        TextView tvImunisasi_Usia;
        TextView tvImunisasi_namaImunisasi;
        TextView tvImunisasi_range_usia;
        TextView tvImunisasi_status;
        Button btnImunisasi_detail;

        String list_id;
        String list_nama;
        String list_dilakukan;
        int jml_sudah_dilakukan;

        public JadwalImunisasiViewHolder(@NonNull View itemView) {
            super(itemView);
            llImunisasi_container = itemView.findViewById(R.id.llImunisasi_container);
            tvImunisasi_Usia = itemView.findViewById(R.id.tvImunisasi_Usia);
            tvImunisasi_namaImunisasi = itemView.findViewById(R.id.tvImunisasi_namaImunisasi);
            tvImunisasi_range_usia = itemView.findViewById(R.id.tvImunisasi_range_usia);
            tvImunisasi_status = itemView.findViewById(R.id.tvImunisasi_status);
            btnImunisasi_detail = itemView.findViewById(R.id.btnImunisasi_detail);
        }
    }
}
