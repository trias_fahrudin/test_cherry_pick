package com.example.sipanji.ui.jadwal_imunisasi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class JadwalImunisasiModelList {

    @SerializedName("jadwalImunisasi")
    private ArrayList<JadwalImunisasiModel> jadwalImunisasiList;

    public ArrayList<JadwalImunisasiModel> getJadwalImunisasiArrayList() {
        return jadwalImunisasiList;
    }

    public void setJadwalImunisasiArraylList(ArrayList<JadwalImunisasiModel> jadwalArrayList) {
        this.jadwalImunisasiList = jadwalArrayList;
    }

}
