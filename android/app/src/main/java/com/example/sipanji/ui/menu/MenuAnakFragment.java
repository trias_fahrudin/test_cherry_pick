package com.example.sipanji.ui.menu;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.ui.antrian.AntrianFragment;
import com.example.sipanji.ui.jadwal_imunisasi.JadwalImunisasiFragment;
import com.example.sipanji.ui.perkembangan.PerkembanganFragment;
import com.example.sipanji.ui.riwayat.RiwayatFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuAnakFragment extends Fragment {

    GridLayout mainGrid;
    @BindView(R.id.menu_antrian)
    CardView menuAntrian;
    @BindView(R.id.menu_perkembangan)
    CardView menuPerkembangan;
    @BindView(R.id.menu_jadwal_imunisasi)
    CardView menuJadwalImunisasi;
    @BindView(R.id.menu_riwayat_pemeriksaan)
    CardView menuRiwayatPemeriksaan;
    private Context context;
    private int pasien_id;
    private String pasien_jk;
    private String pasien_tgl_lahir;
    private int pasien_usia;
    private String pasien_nama;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_menu_anak, container, false);
        ButterKnife.bind(this, root);

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            pasien_id = getArguments().getInt("pasien_id", 0);
            pasien_jk = getArguments().getString("pasien_jk", "");
            pasien_tgl_lahir = getArguments().getString("pasien_tgl_lahir", "");
            pasien_usia = getArguments().getInt("pasien_usia", 0);
            pasien_nama = getArguments().getString("pasien_nama", "");
        }

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Menu Pemeriksaan " + pasien_nama);
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
        }

        return root;

    }


    @OnClick(R.id.menu_antrian)
    public void onMenuAntrianClicked() {
        Bundle bundle = new Bundle();
        bundle.putInt("pasien_id", this.pasien_id);
        bundle.putString("pasien_tgl_lahir", this.pasien_tgl_lahir);
        bundle.putInt("pasien_usia", this.pasien_usia);
        bundle.putString("pasien_nama",this.pasien_nama);

        AntrianFragment fragment = new AntrianFragment();
        fragment.setArguments(bundle);
        AppCompatActivity activity = (AppCompatActivity) getView().getContext();

        activity.getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.nav_host_fragment, fragment, AntrianFragment.class.getSimpleName())
            .addToBackStack(null)
            .commit();

    }

    @OnClick(R.id.menu_perkembangan)
    public void onMenuPerkembanganClicked() {
        Bundle bundle = new Bundle();
        bundle.putInt("pasien_id", this.pasien_id);
        bundle.putString("pasien_jk", this.pasien_jk);
        bundle.putString("pasien_nama",this.pasien_nama);

        PerkembanganFragment fragment = new PerkembanganFragment();
        fragment.setArguments(bundle);
        AppCompatActivity activity = (AppCompatActivity) getView().getContext();

        activity.getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.nav_host_fragment, fragment, PerkembanganFragment.class.getSimpleName())
            .addToBackStack(null)
            .commit();
    }

    @OnClick(R.id.menu_jadwal_imunisasi)
    public void onMenuJadwalImunisasiClicked() {

        Bundle bundle = new Bundle();
        bundle.putInt("pasien_id", this.pasien_id);
        bundle.putString("pasien_tgl_lahir", this.pasien_tgl_lahir);
        bundle.putInt("pasien_usia", this.pasien_usia);
        bundle.putString("pasien_nama",this.pasien_nama);

        JadwalImunisasiFragment fragment = new JadwalImunisasiFragment();
        fragment.setArguments(bundle);
        AppCompatActivity activity = (AppCompatActivity) getView().getContext();

        activity.getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.nav_host_fragment, fragment, JadwalImunisasiFragment.class.getSimpleName())
            .addToBackStack(null)
            .commit();
    }

    @OnClick(R.id.menu_riwayat_pemeriksaan)
    public void onMenuRiwayatPemeriksaanClicked() {

        Bundle bundle = new Bundle();
        bundle.putInt("pasien_id", this.pasien_id);
        bundle.putString("pasien_nama",this.pasien_nama);

        RiwayatFragment fragment = new RiwayatFragment();
        fragment.setArguments(bundle);
        AppCompatActivity activity = (AppCompatActivity) getView().getContext();

        activity.getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.nav_host_fragment, fragment, RiwayatFragment.class.getSimpleName())
            .addToBackStack(null)
            .commit();
    }
}
