package com.example.sipanji.ui.perkembangan;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.sipanji.R;

public class PerkembanganTabsPagerAdapter extends FragmentStatePagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_grafik_bb, R.string.tab_grafik_tb, R.string.tab_grafik_lk};
    private final Context mContext;
    private final String pasien_jk;
    private int pasien_id;

    public PerkembanganTabsPagerAdapter(int pasien_id,String pasien_jk, Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        this.pasien_id = pasien_id;
        this.pasien_jk = pasien_jk;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return GrafikBBFragment.newInstance(this.pasien_id,this.pasien_jk);
            case 1:
                return GrafikTBFragment.newInstance(this.pasien_id,this.pasien_jk);
            case 2:
                return GrafikLKFragment.newInstance(this.pasien_id,this.pasien_jk);
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
