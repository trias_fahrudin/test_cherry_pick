package com.example.sipanji.ui.perkembangan_detail;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sipanji.R;

import java.util.ArrayList;
import java.util.Locale;

// tgl periksa (usia: ...)
// BB:.. // TB : .. // LK:..
// Status:
//  Berat/Tinggi : ...
//  Berat/Usia : ...
//  IMT/Usia : ...
//  Lingkar Kepala/Usia: ...
//  Tinggi/Usia: ...
public class PerkembanganDetailAdapter extends RecyclerView.Adapter<PerkembanganDetailAdapter.PerkembanganDetailViewHolder> {

    private ArrayList<PerkembanganDetailModel> dataList;

    public PerkembanganDetailAdapter(ArrayList<PerkembanganDetailModel> dataList) {
        this.dataList = dataList;
    }


    @NonNull
    @Override
    public PerkembanganDetailAdapter.PerkembanganDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_detail_perkembangan_list, parent, false);
        return new PerkembanganDetailAdapter.PerkembanganDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PerkembanganDetailAdapter.PerkembanganDetailViewHolder holder, int position) {

        String tglPeriksa = dataList.get(position).getTgl_periksa();
        int usia = dataList.get(position).getMonth_diff();
        holder.tvTglPeriksa.setText(String.format(Locale.US, "Tanggal Periksa : %s ( %d bulan )", tglPeriksa, usia));

        double bb = dataList.get(position).getBerat_badan();
        double tb = dataList.get(position).getTinggi_badan();
        double lk = dataList.get(position).getLingkar_kepala();
        holder.tvAntropometri.setText(String.format(Locale.US, "Berat: %.1f Kg // Tinggi: %.1f Cm // Lingkar Kepala: %.1f Cm", bb, tb, lk));

        String status_bb_pb = dataList.get(position).getStatus_bb_pb();
        String status_bb_tb = dataList.get(position).getStatus_bb_tb();
        String status_bb_u = dataList.get(position).getStatus_bb_u();
        String status_imt_u_0_60 = dataList.get(position).getStatus_imt_u_0_60();
        String status_imt_u_61_72 = dataList.get(position).getStatus_imt_u_61_72();
        String status_lk_u = dataList.get(position).getStatus_lk_u();
        String status_pb_u = dataList.get(position).getStatus_pb_u();
        String status_tb_u = dataList.get(position).getStatus_tb_u();

        if (usia <= 24) {
            //panjang badan
            holder.tvStatus.setText(String.format(Locale.US, "Berat/Panjang Badan: %s\nBerat/Usia: %s\nIMT/Usia: %s\nLingkar Kepala/Usia: %s\nPanjang Badan/Usia: %s", status_bb_pb, status_bb_u, status_imt_u_0_60, status_lk_u, status_pb_u));
        } else {
            if (usia <= 60) {
                holder.tvStatus.setText(String.format(Locale.US, "Berat/Tinggi Badan: %s\nBerat/Usia: %s\nIMT/Usia: %s\nLingkar Kepala/Usia: %s\nTinggi Badan/Usia: %s", status_bb_tb, status_bb_u, status_imt_u_0_60, status_lk_u, status_tb_u));
            } else {
                holder.tvStatus.setText(String.format(Locale.US, "Berat/Tinggi Badan: %s\nBerat/Usia: %s\nIMT/Usia: %s\nLingkar Kepala/Usia: %s\nTinggi Badan/Usia: %s", status_bb_tb, status_bb_u, status_imt_u_61_72, status_lk_u, status_tb_u));
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class PerkembanganDetailViewHolder extends RecyclerView.ViewHolder {
        TextView tvTglPeriksa;
        TextView tvAntropometri;
        TextView tvStatus;

        public PerkembanganDetailViewHolder(View itemView) {
            super(itemView);
            tvTglPeriksa = itemView.findViewById(R.id.rdp_tvTglPeriksa);
            tvAntropometri = itemView.findViewById(R.id.rdp_tvAntropometri);
            tvStatus = itemView.findViewById(R.id.rdp_tvStatus);
        }
    }
}
