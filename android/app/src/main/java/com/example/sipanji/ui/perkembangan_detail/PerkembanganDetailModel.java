package com.example.sipanji.ui.perkembangan_detail;

import com.google.gson.annotations.SerializedName;

public class PerkembanganDetailModel {

    @SerializedName("tgl_periksa")
    private String tgl_periksa;
    @SerializedName("berat_badan")
    private double berat_badan;
    @SerializedName("tinggi_badan")
    private double tinggi_badan;
    @SerializedName("lingkar_kepala")
    private double lingkar_kepala;
    @SerializedName("month_diff")
    private int month_diff;
    @SerializedName("status_bb_pb")
    private String status_bb_pb;
    @SerializedName("status_bb_tb")
    private String status_bb_tb;
    @SerializedName("status_bb_u")
    private String status_bb_u;
    @SerializedName("status_imt_u_0_60")
    private String status_imt_u_0_60;
    @SerializedName("status_imt_u_61_72")
    private String status_imt_u_61_72;
    @SerializedName("status_lk_u")
    private String status_lk_u;
    @SerializedName("status_pb_u")
    private String status_pb_u;
    @SerializedName("status_tb_u")
    private String status_tb_u;

    public String getTgl_periksa() {
        return tgl_periksa;
    }

    public void setTgl_periksa(String tgl_periksa) {
        this.tgl_periksa = tgl_periksa;
    }

    public double getBerat_badan() {
        return berat_badan;
    }

    public void setBerat_badan(double berat_badan) {
        this.berat_badan = berat_badan;
    }

    public double getTinggi_badan() {
        return tinggi_badan;
    }

    public void setTinggi_badan(double tinggi_badan) {
        this.tinggi_badan = tinggi_badan;
    }

    public double getLingkar_kepala() {
        return lingkar_kepala;
    }

    public void setLingkar_kepala(double lingkar_kepala) {
        this.lingkar_kepala = lingkar_kepala;
    }

    public int getMonth_diff() {
        return month_diff;
    }

    public void setMonth_diff(int month_diff) {
        this.month_diff = month_diff;
    }

    public String getStatus_bb_pb() {
        return status_bb_pb;
    }

    public void setStatus_bb_pb(String status_bb_pb) {
        this.status_bb_pb = status_bb_pb;
    }

    public String getStatus_bb_tb() {
        return status_bb_tb;
    }

    public void setStatus_bb_tb(String status_bb_tb) {
        this.status_bb_tb = status_bb_tb;
    }

    public String getStatus_bb_u() {
        return status_bb_u;
    }

    public void setStatus_bb_u(String status_bb_u) {
        this.status_bb_u = status_bb_u;
    }

    public String getStatus_imt_u_0_60() {
        return status_imt_u_0_60;
    }

    public void setStatus_imt_u_0_60(String status_bb_imt_u_0_60) {
        this.status_imt_u_0_60 = status_bb_imt_u_0_60;
    }

    public String getStatus_imt_u_61_72() {
        return status_imt_u_61_72;
    }

    public void setStatus_imt_u_61_72(String status_bb_imt_u_61_72) {
        this.status_imt_u_61_72 = status_bb_imt_u_61_72;
    }

    public String getStatus_lk_u() {
        return status_lk_u;
    }

    public void setStatus_lk_u(String status_lk_u) {
        this.status_lk_u = status_lk_u;
    }

    public String getStatus_pb_u() {
        return status_pb_u;
    }

    public void setStatus_pb_u(String status_pb_u) {
        this.status_pb_u = status_pb_u;
    }

    public String getStatus_tb_u() {
        return status_tb_u;
    }

    public void setStatus_tb_u(String status_tb_u) {
        this.status_tb_u = status_tb_u;
    }


}
