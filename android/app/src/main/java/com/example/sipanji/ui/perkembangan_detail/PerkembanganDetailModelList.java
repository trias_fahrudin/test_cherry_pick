package com.example.sipanji.ui.perkembangan_detail;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PerkembanganDetailModelList {

    @SerializedName("perkembanganList")
    private ArrayList<PerkembanganDetailModel> perkembanganDetailList;

    public ArrayList<PerkembanganDetailModel> getArrayList() {
        return perkembanganDetailList;
    }

    public void setArraylList(ArrayList<PerkembanganDetailModel> perkembanganDetailList) {
        this.perkembanganDetailList = perkembanganDetailList;
    }

}
