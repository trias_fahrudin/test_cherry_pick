package com.example.sipanji.ui.riwayat;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RiwayatImunisasiModelList {

    @SerializedName("riwayatImunisasiList")
    private ArrayList<RiwayatImunisasiModel> riwayatImunisasiList;

    public ArrayList<RiwayatImunisasiModel> getRiwayatImunisasiArrayList() {
        return riwayatImunisasiList;
    }

    public void setArraylList(ArrayList<RiwayatImunisasiModel> riwayatImunisasiArrayList) {
        this.riwayatImunisasiList = riwayatImunisasiArrayList;
    }

}
