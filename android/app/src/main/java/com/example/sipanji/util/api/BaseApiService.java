package com.example.sipanji.util.api;

import com.example.sipanji.ui.anak.AnakModelList;
import com.example.sipanji.ui.antrian.AntrianModelList;
import com.example.sipanji.ui.jadwal_imunisasi.JadwalImunisasiModelList;
import com.example.sipanji.ui.perkembangan_detail.PerkembanganDetailModelList;
import com.example.sipanji.ui.riwayat.RiwayatImunisasiModelList;
import com.example.sipanji.ui.riwayat.RiwayatPemeriksaanModelList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface BaseApiService {
    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> login(@Field("username") String username,
                             @Field("password") String password);

    @FormUrlEncoded
    @POST("post-daftar-antrian")
    Call<ResponseBody> postPendaftaranAntrian(@Field("jenis") String jenis,
                                              @Field("tanggal") String tanggal,
                                              @Field("pasien_id") int pasien_id);

    @GET("get-antrian")
    Call<AntrianModelList> getAntrian(@Query("pasien_id") int pasien_id);

    @FormUrlEncoded
    @POST("post-batalkan-antrian")
    Call<ResponseBody> postBatalkanAntrian(@Field("antrian_id") int antrian_id);


    @GET("get-antrian-detail")
    Call<ResponseBody> getAntrianDetail(@Query("antrian_id") int antrian_id);

    @GET("get-anak")
    Call<AnakModelList> getAnak(@Query("ortu_id") int ortu_id);

    @GET("get-riwayat-imunisasi")
    Call<RiwayatImunisasiModelList> getRiwayatImunisasi(@Query("pasien_id") int pasien_id);

    @GET("get-riwayat-pemeriksaan")
    Call<RiwayatPemeriksaanModelList> getRiwayatPemeriksaan(@Query("pasien_id") int pasien_id);

    @FormUrlEncoded
    @POST("send_tokenid")
    Call<ResponseBody> sendRegistrationToServer(@Field("ortu_id") int ortu_id,
                                                @Field("token_id") String token_id);

//    okhttp3.Call getJadwalImunisasi(int pasien_id, String pasien_tgl_lahir, int pasien_usia);

    @GET("get-jadwal-imunisasi")
    Call<JadwalImunisasiModelList> getJadwalImunisasi(@Query("pasien_id") int pasien_id,
                                                      @Query("pasien_tgl_lahir") String pasien_tgl_lahir,
                                                      @Query("pasien_usia") int pasien_usia);

    @GET("get-perkembangan-detail")
    Call<PerkembanganDetailModelList> getPerkembanganDetail(@Query("pasien_id") int pasien_id);

    @FormUrlEncoded
    @POST("post-foto-profile")
    Call<ResponseBody> postFotoProfile(@Field("pasien_id") int pasien_id,
                                       @Field("foto") String foto);
}
