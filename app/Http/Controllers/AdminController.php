<?php

namespace App\Http\Controllers;

use App\Pengguna;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function pengguna()
    {
        $pengguna = Pengguna::all();
        return view('admin.pengguna.list', ['pengguna' => $pengguna]);
    }

    public function penggunaHapus($id)
    {
        $pengguna = Pengguna::find($id);
        $pengguna->delete();
        return redirect(URL::to('/admin/pengguna'));
    }

    public function laporanPasien(Request $request)
    {
        switch ($request->method()) {
            case 'POST':
                $status = $request->status;

                $having = "";

                if ($status === "semua") {

                } elseif ($status === "aktif") {
                    $having = "HAVING `status` = 'AKTIF'";
                } else {
                    $having = "HAVING `status` = 'TIDAK AKTIF'";
                }

                $data = DB::select("
                    SELECT a.pasien_id,
                            b.nama,
                            CONCAT(c.nama_ayah,' - ', c.nama_ibu) AS orangtua,
                            MAX(DATE(a.tgl)) AS tgl_terakhir,
                            DATEDIFF(DATE(NOW()),MAX(a.tgl)) AS date_diff,
                            IF( (DATEDIFF(DATE(NOW()),MAX(a.tgl))) > (365*3),'TIDAK AKTIF','AKTIF') AS `status`
                    FROM antrian_dokter a
                    LEFT JOIN pasien b ON a.pasien_id = b.id
                    LEFT JOIN ortu c ON b.ortu_id = c.id
                    GROUP BY a.pasien_id " . $having);

                return view('admin.laporan_pasien', ['data' => $data, 'status' => $status]);

                break;
            case 'GET':
                return view('admin.laporan_pasien', ['status' => 'semua']);
                break;

            default:
                # code...
                break;
        }
    }

    public function laporanKesesuaianImunisasi(Request $request)
    {

        switch ($request->method()) {
            case 'POST':

                //header('content-type: application/json');
                

                $status = $request->filter;

                $data = DB::select("
                    SELECT b.nama AS nama_pasien,
                           a.pasien_id,
                           TIMESTAMPDIFF(MONTH, b.tgl_lahir,DATE(NOW())) AS usia,
                           IF( (DATEDIFF(DATE(NOW()),MAX(a.tgl))) > (365*3),'TIDAK AKTIF','AKTIF') AS `status`
                    FROM antrian_dokter a
                    LEFT JOIN pasien b ON a.pasien_id = b.id
                    LEFT JOIN ortu c ON b.ortu_id = c.id
                    GROUP BY a.pasien_id HAVING `status` = 'AKTIF'");

                if ($status === 'SESUAI') {

                    

                    $result = array();

                    foreach ($data as $d) {

                        $imunisasi = DB::select(
                            "SELECT GROUP_CONCAT(a.nama) AS nama_imunisasi
                             FROM jenis_imunisasi a
                             LEFT JOIN imunisasi b ON a.id = b.jenis_imunisasi_id AND b.pasien_id = :pasien_id
                             LEFT JOIN pasien c ON b.pasien_id = c.id
                             WHERE a.usia < :usia_pasien AND c.nama IS NOT NULL",
                            array(
                                'pasien_id'   => $d->pasien_id,
                                'usia_pasien' => $d->usia)
                        );

                        foreach ($imunisasi as $im) {
                            $arr['imunisasi'] = $im->nama_imunisasi;
                            $arr['pasien']    = $d->nama_pasien;
                            $arr['usia']      = $d->usia;

                            array_push($result, $arr);
                        }

                    }

                    // echo json_encode(
                    //     array(
                    //         'error'      => false,
                    //         'message'    => 'Data berhasil diambil',
                    //         'pasienList' => $result,
                    //     )
                    // );

                    // var_dump($result);

                    return view('admin.laporan_kesesuaian_imunisasi', [
                        'data'   => $result,
                        'filter' => $status,]
                    );

                } elseif ($status === 'TIDAK-SESUAI') {

                    $result = array();

                    foreach ($data as $d) {

                        $imunisasi = DB::select(
                            "SELECT GROUP_CONCAT(a.nama) AS nama_imunisasi
                             FROM jenis_imunisasi a
                             LEFT JOIN imunisasi b ON a.id = b.jenis_imunisasi_id AND b.pasien_id = :pasien_id
                             LEFT JOIN pasien c ON b.pasien_id = c.id
                             WHERE a.usia < :usia_pasien AND c.nama IS NULL",
                            array(
                                'pasien_id'   => $d->pasien_id,
                                'usia_pasien' => $d->usia)
                        );

                        foreach ($imunisasi as $im) {
                            $arr['imunisasi'] = $im->nama_imunisasi;
                            $arr['pasien']    = $d->nama_pasien;
                            $arr['usia']      = $d->usia;

                            array_push($result, $arr);
                        }

                    }

                    // var_dump($result);

                    return view('admin.laporan_kesesuaian_imunisasi', [
                        'data'   => $result,
                        'filter' => $status,]
                    );
                }

                break;
            case 'GET':
                return view('admin.laporan_kesesuaian_imunisasi', ['filter' => 'SEMUA']);
                break;

            default:
                # code...
                break;
        }

    }

    public function laporanImunisasi(Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $tahun = $request->tahun;
                $bulan = $request->bulan;

                $data = DB::select("
                    SELECT a.tgl,b.nama AS nama_pasien,c.nama AS nama_imunisasi,a.keterangan,a.resep
                    FROM imunisasi a
                    LEFT JOIN pasien b ON a.pasien_id = b.id
                    LEFT JOIN jenis_imunisasi c ON a.jenis_imunisasi_id = c.id
                    WHERE YEAR(a.tgl) = :tahun AND MONTH(a.tgl) = :bulan",
                    array(
                        'tahun' => $tahun,
                        'bulan' => $bulan)
                );

                $imunisasi = DB::select(
                    "SELECT b.nama AS jenis,
                            COUNT(a.id) AS jml
                     FROM imunisasi a
                     LEFT JOIN jenis_imunisasi b ON a.jenis_imunisasi_id = b.id
                     WHERE YEAR(a.tgl) = :tahun AND MONTH(a.tgl) = :bulan
                     GROUP BY b.id",
                    array(
                        'tahun' => $tahun,
                        'bulan' => $bulan)
                );

                return view('admin.laporan_imunisasi', [
                    
                    'data'           => $data,
                    'imunisasi'      => $imunisasi,
                    'bulan_sekarang' => $bulan,
                    'tahun_sekarang' => $tahun]
                );

                break;
            case 'GET':

                $now   = Carbon::now();
                $tahun = $now->year;
                $bulan = $now->month;

                return view('admin.laporan_imunisasi', [
                    
                    'bulan_sekarang' => $bulan,
                    'tahun_sekarang' => $tahun]
                );

                break;

            default:
                # code...
                break;
        }
    }

    public function laporanPemeriksaan(Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $tahun = $request->tahun;
                $bulan = $request->bulan;

                $data = DB::select("
                    SELECT a.tgl,b.nama AS nama_pasien,a.anamnesa,a.diagnosa,a.tindakan,a.resep
                    FROM pemeriksaan a
                    LEFT JOIN pasien b ON a.pasien_id = b.id
                    WHERE YEAR(a.tgl) = :tahun AND MONTH(a.tgl) = :bulan",
                    array(
                        'tahun' => $tahun,
                        'bulan' => $bulan)
                );

                return view('admin.laporan_pemeriksaan', [
                    'data'           => $data,
                    'bulan_sekarang' => $bulan,
                    'tahun_sekarang' => $tahun]
                );

                break;
            case 'GET':

                $now   = Carbon::now();
                $tahun = $now->year;
                $bulan = $now->month;

                return view('admin.laporan_pemeriksaan', [
                    'bulan_sekarang' => $bulan,
                    'tahun_sekarang' => $tahun]
                );

                break;

            default:
                # code...
                break;
        }
    }

    public function laporanGrafik(Request $request)
    {
        switch ($request->method()) {
            case 'POST':
                $tahun = $request->tahun;

                $data = DB::select("
                    SELECT a.kode,
                           a.nama,
                             IFNULL(b.jml,0) AS jml_imunisasi,
                             IFNULL(c.jml,0) AS jml_pemeriksaan
                    FROM bulan a
                    LEFT JOIN (SELECT COUNT(id) AS jml,tgl
                               FROM imunisasi
                               WHERE YEAR(tgl) = '$tahun'
                                  GROUP BY MONTH(tgl)) b ON a.kode = MONTH(b.tgl)
                    LEFT JOIN (SELECT COUNT(id) AS jml,tgl
                               FROM pemeriksaan
                               WHERE YEAR(tgl) = '$tahun'
                                  GROUP BY MONTH(tgl)) c ON a.kode = MONTH(c.tgl)
                    GROUP BY a.kode");

                return view('admin.laporan_grafik', [
                    'data'           => $data,
                    'tahun_sekarang' => $tahun]
                );

                break;
            case 'GET':

                $now   = Carbon::now();
                $tahun = $now->year;

                return view('admin.laporan_grafik', [
                    'tahun_sekarang' => $tahun]
                );

                break;

            default:
                # code...
                break;
        }
    }

    public function profile(Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'  => 'required',
                    'telp'  => 'required',
                    'email' => 'email:rfc',

                ]);

                $pengguna        = Pengguna::find(Session::get('user_id'));
                $pengguna->nama  = $request->nama;
                $pengguna->telp  = $request->telp;
                $pengguna->email = $request->email;

                $pengguna->save();
                return redirect(URL::to('/admin/profile'))->with('alert-success', 'Profile berhasil disimpan');
                // return $redirect->to('/admin/profile')->with('alert-danger', 'Password atau Email salah !')->send();

                break;
            case 'GET':

                $pengguna = Pengguna::find(Session::get('user_id'));
                return view('admin.profile', ['pengguna' => $pengguna]);

                break;

            default:
                # code...
                break;
        }
    }

    public function penggunaEdit($id, Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'  => 'required',
                    'telp'  => 'required',
                    'email' => 'email:rfc',
                    'level' => 'required',
                ]);

                $pengguna        = Pengguna::find($id);
                $pengguna->nama  = $request->nama;
                $pengguna->telp  = $request->telp;
                $pengguna->email = $request->email;
                $pengguna->level = $request->level;

                $pengguna->save();
                return redirect(URL::to('/admin/pengguna'));

                break;
            case 'GET':

                $pengguna = Pengguna::find($id);
                return view('admin.pengguna.edit', ['pengguna' => $pengguna]);

                break;

            default:
                # code...
                break;
        }
    }

    public function penggunaTambah(Request $request)
    {

        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'username' => 'required',
                    'password' => 'required',
                    'nama'     => 'required',
                    'telp'     => 'required',
                    'email'    => 'email:rfc',
                    'level'    => 'required',
                ]);

                Pengguna::create([
                    'username' => $request->username,
                    'password' => md5($request->password),
                    'nama'     => $request->nama,
                    'telp'     => $request->telp,
                    'email'    => $request->email,
                    'level'    => $request->level,

                ]);

                return redirect(URL::to('/admin/pengguna'));

                break;
            case 'GET':

                return view('admin.pengguna.tambah');

                break;

            default:
                # code...
                break;
        }

    }

}
