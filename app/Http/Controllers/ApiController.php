<?php

namespace App\Http\Controllers;

use App\AntrianDokter;
use App\Ortu;
use App\Pasien;
use App\Traits\HelperFunction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Intervention\Image\ImageManagerStatic as Image;
use peal\barcodegenerator\Facades\BarCode;

class ApiController extends Controller
{

    use HelperFunction;

    /*
    #
    Route::post('post-daftar-antrian','ApiController@postDaftarAntrian');
    Route::get('get-antrian','ApiController@getAntrian');
    Route::post('post-batalkan-antrian','ApiController@postBatalkanAntrian');
    Route::get('get-antrian-detail','ApiController@getAntrianDetail');

    #
    Route::get('get-riwayat-notifikasi','ApiController@getRiwayatNotifikasi');

     */

    public function postFotoProfile(Request $request)
    {

        header('content-type: application/json');
        $pasien_id = $request->pasien_id;

        $foto = $request->file('foto');

        $image     = $request->foto; // your base64 encoded
        $image     = str_replace('data:image/png;base64,', '', $image);
        $image     = str_replace(' ', '+', $image);
        $imageName = 'pasien-' . $pasien_id . '.png';
        // \File::put(storage_path() . '/' . $imageName, base64_decode($image));

        $path = public_path() . '/data_file/' . $imageName;

        //Storage::disk('public')->put($imageName, base64_decode($image));
        Image::make(base64_decode($image))->save($path);

        $pasien = Pasien::find($pasien_id);

        $pasien->foto = $imageName;

        $pasien->save();

        $response["error"]     = false;
        $response["error_msg"] = "Upload foto berhasil";

        echo json_encode($response);

    }

    public function postDaftarAntrian(Request $request)
    {

        header('content-type: application/json');

        $jenis       = $request->jenis;
        $tgl_antrian = $request->tanggal;
        $pasien_id   = $request->pasien_id;

        $date = new \DateTime($tgl_antrian);
        $now  = new \DateTime();

        // if ($date <= $now) {

        //     $response["error"]     = true;
        //     $response["error_msg"] = "Tanggal pemesanan dimulai dari besok";

        //     echo json_encode($response);
        // } else {

        $antrian = DB::select("SELECT id
                                   FROM antrian_dokter
                                   WHERE DATE(tgl) = '$tgl_antrian'
                                         AND pasien_id=$pasien_id
                                         AND jenis = '$jenis'");

        if (count($antrian) > 0) {
            $response["error"]     = true;
            $response["error_msg"] = "Anda sudah melakukan pemesanan antrian ditanggal ini";

            echo json_encode($response);
        } else {

            $jmlAntrian = DB::select("SELECT id
                                          FROM antrian_dokter
                                          WHERE DATE(tgl) = '$tgl_antrian'");

            if (count($jmlAntrian) >= 60) {

                $response["error"]     = true;
                $response["error_msg"] = "Jumlah antrian untuk hari terpilih sudah penuh";

                echo json_encode($response);

            } else {
                AntrianDokter::create([
                    'jenis'     => $jenis,
                    'tgl'       => $tgl_antrian . ' ' . Carbon::now()->format('h:i:s'),
                    'pasien_id' => $pasien_id,
                    'status'    => 'antri',

                ]);

                //DB::statement('CALL update_antrian()');
                $this->update_antrian();

                $response["error"]     = false;
                $response["error_msg"] = "Pendaftaran Antrian Berhasil";

                echo json_encode($response);
            }

        }

        // }

    }

    public function getAntrian(Request $request)
    {

        header('content-type: application/json');

        $pasien_id = $request->pasien_id;
        $antrian   = DB::select("SELECT id,jenis,DATE_FORMAT(DATE(tgl), '%d/%m/%Y') AS tgl
                                 FROM antrian_dokter
                                 WHERE DATE(tgl) >= DATE(NOW()) AND pasien_id = $pasien_id");

        echo json_encode(
            array(
                'error'       => false,
                'message'     => 'Data berhasil diambil',
                'antrianList' => $antrian,
            )
        );

    }

    public function postBatalkanAntrian(Request $request)
    {
        header('content-type: application/json');

        $antrian_id = $request->antrian_id;

        $antrian = AntrianDokter::find($antrian_id);
        $antrian->delete();

        $response["error"]     = false;
        $response["error_msg"] = "Antrian Berhasil Dihapus";

        echo json_encode($response);

    }

    public function getAntrianDetail(Request $request)
    {

        header('content-type: application/json');

        $id = $request->antrian_id;

        $antrian       = DB::select("SELECT antrian_ke FROM antrian_dokter WHERE id = $id")[0];
        $pasien        = DB::select("SELECT pasien_id FROM antrian_dokter WHERE id = $id")[0];
        $antrian_aktif = DB::select("SELECT antrian_ke FROM antrian_dokter WHERE DATE(tgl) = DATE(NOW())  AND `status` IN('diperiksa','selesai') ORDER BY id DESC LIMIT 1 ");

        //generate barcode
        $barcode = [
            'text'        => ('PASIEN:' . $pasien->pasien_id),
            'size'        => 60,
            'orientation' => 'horizontal',
            'code_type'   => 'code128',
            'print'       => true,
            'sizefactor'  => 1,
            'filename'    => 'image' . $id . '.jpeg',
        ];

        $barcontent = BarCode::barcodeFactory()->renderBarcode(
            $text = $barcode["text"],
            $size = $barcode['size'],
            $orientation = $barcode['orientation'],
            $code_type = $barcode['code_type'], // code_type : code128,code39,code128b,code128a,code25,codabar
            $print = $barcode['print'],
            $sizefactor = $barcode['sizefactor'],
            $filename = $barcode['filename']
        )->filename($barcode['filename']);

        // echo '<img alt="testing" src="'.$barcontent.'"/>';

        // \QrCode::size(500)
        //     ->format('png')
        //     ->generate('HDTuto.com', public_path('images/qrcode.png'));

        $return['error']        = 'false';
        $return['barcode_file'] = URL::to($barcontent);
        $return['urutan_ke']    = $antrian->antrian_ke;
        if (count($antrian_aktif) > 0) {
            $return['pasien_diperiksa'] = $antrian_aktif[0]->antrian_ke;
        } else {
            $return['pasien_diperiksa'] = 0;
        }

        echo json_encode($return);

    }

    public function updateAntrian()
    {
        $this->update_antrian();
    }

    public function getKabupaten(Request $request)
    {

        $prov_id = $request->prov;
        $kab_id  = $request->kab;

        $html = "<option value=''>Pilih Kabupaten</option>";

        $row = DB::select('SELECT * FROM wil_kab WHERE prov_id = ?', [$prov_id]);

        foreach ($row as $r) {
            if (isset($kab_id)) {
                if ($kab_id == $r->id) {
                    $html .= "<option selected value='$r->id'>$r->nama</option>";
                } else {
                    $html .= "<option value='$r->id'>$r->nama</option>";
                }
            } else {
                $html .= "<option value='$r->id'>$r->nama</option>";
            }
        }

        echo $html;
    }

    public function getKecamatan(Request $request)
    {

        $kab_id = $request->kab;
        $kec_id = $request->kec;

        $html = "<option value=''>Pilih Kecamatan</option>";

        $row = DB::select('SELECT * FROM wil_kec WHERE kab_id = ?', [$kab_id]);

        foreach ($row as $r) {
            if (isset($kec_id)) {
                if ($kec_id == $r->id) {
                    $html .= "<option selected value='$r->id'>$r->nama</option>";
                } else {
                    $html .= "<option value='$r->id'>$r->nama</option>";
                }
            } else {
                $html .= "<option value='$r->id'>$r->nama</option>";
            }
        }

        echo $html;
    }

    public function getProvinsi()
    {

        header('content-type: application/json');

        $prov = DB::select("SELECT id,nama FROM wil_prov");

    }

    public function getDaftarObat(Request $request)
    {

        header('content-type: application/json');

        $nama_obat = $request->q;

        $obat = DB::select("SELECT nama AS `key`,nama AS value FROM obat WHERE nama LIKE '%$nama_obat%' ");

        echo json_encode($obat); //json_encode($obat);
    }

    public function login(Request $request)
    {

        header('content-type: application/json');

        $username = $request->username;
        $password = $request->password;

        $user = Ortu::where('username', $username)
            ->where('password', md5($password));

        if ($user->count() > 0) {

            $response["error"]                = false;
            $response["user"]["id"]           = $user->first()->id;
            $response["user"]["username"]     = $user->first()->username;
            $response["user"]["nama_lengkap"] = $user->first()->nama_ibu . '-' . $user->first()->nama_ayah;
            $response["user"]["email"]        = $user->first()->email;

            echo json_encode($response);
        } else {
            $response["error"]     = true;
            $response["error_msg"] = "Periksa kembali username dan password anda";
            echo json_encode($response);
        }

    }

    public function sendTokenId(Request $request)
    {
        $ortu_id  = $request->ortu_id;
        $token_id = $request->token_id;

        DB::table('ortu')
            ->where('id', $ortu_id)
            ->update(['token_id' => $token_id]);

        $response["error"]     = false;
        $response["error_msg"] = "Send Token BERHASIL";

        echo json_encode($response);
    }

    public function getAnak(Request $request)
    {

        $ortu_id = $request->ortu_id;
        $pasien  = DB::select(
            "SELECT a.id,IFNULL(foto,'no_foto.png') AS foto,
                    a.nama AS nama,
                    a.tgl_lahir AS tgl_lahir,
                    TIMESTAMPDIFF(MONTH,a.tgl_lahir, NOW()) AS usia,
                    CONCAT(b.nama_ayah,' - ',b.nama_ibu) AS ortu,
                    a.jk,
                    b.alamat
            FROM pasien AS a
            LEFT JOIN ortu AS b ON a.ortu_id = b.id
            WHERE a.ortu_id = $ortu_id
            ORDER BY a.nama ASC");

        echo json_encode(
            array(
                'error'    => false,
                'message'  => 'Data berhasil diambil',
                'anakList' => $pasien,
            )
        );
    }

    public function getPerkembanganDetail(Request $request)
    {

        // perkembanganList
        $pasien_id    = $request->pasien_id;
        $perkembangan = DB::select("SELECT DATE_FORMAT(DATE(tgl_periksa),'%d-%m-%Y') AS tgl_periksa,
                                           berat_badan,
                                           tinggi_badan,
                                           lingkar_kepala,
                                           month_diff,
                                           status_bb_pb,
                                           status_bb_tb,
                                           status_bb_u,
                                           status_imt_u_0_60,
                                           status_imt_u_61_72,
                                           status_lk_u,
                                           status_pb_u,
                                           status_tb_u
                                    FROM view_perkembangan
                                    WHERE pasien_id = $pasien_id ORDER BY tgl_periksa DESC");

        echo json_encode(
            array(
                'error'            => false,
                'message'          => 'Data berhasil diambil',
                'perkembanganList' => $perkembangan,
            )
        );

    }

    public function getJadwalImunisasi(Request $request)
    {

        $pasien_id        = $request->pasien_id;
        $pasien_tgl_lahir = $request->pasien_tgl_lahir;

        // $jadwal = DB::select("
        //     SELECT a.usia AS imunisasi_usia,
        //              GROUP_CONCAT(a.id) AS imunisasi_id,
        //            GROUP_CONCAT(a.nama ORDER BY a.nama ASC) AS imunisasi_nama,
        //            IFNULL(
        //            CONCAT(
        //                 DATE_FORMAT(DATE_ADD('$pasien_tgl_lahir', INTERVAL a.usia MONTH),'%d/%m/%Y') , ' s/d ',
        //                 DATE_FORMAT(DATE_ADD(DATE_ADD('$pasien_tgl_lahir', INTERVAL a.usia MONTH), INTERVAL a.max_toleransi DAY),'%d/%m/%Y')
        //            ),0) AS range_usia,
        //            CONCAT(COUNT(b.id), ' dari ' , COUNT(a.id))  AS imunisasi_status,
        //            GROUP_CONCAT(CONCAT(b.jenis_imunisasi_id,'::',b.tgl)) AS imunisasi_dilakukan,
        //            (COUNT(a.id) - COUNT(b.id)) AS jml_belum_dilakukan
        //     FROM jenis_imunisasi a
        //     LEFT JOIN imunisasi b ON a.id = b.jenis_imunisasi_id AND b.pasien_id = $pasien_id
        //     GROUP BY usia");
        $jadwal = DB::select("
            SELECT a.usia,
                   GROUP_CONCAT(a.id ORDER BY a.nama) AS imunisasi_id,
                   GROUP_CONCAT(a.nama ORDER BY a.nama ASC) AS imunisasi_nama,
                   IFNULL(
                   CONCAT(
                        DATE_FORMAT(DATE_ADD('$pasien_tgl_lahir', INTERVAL a.usia MONTH),'%d-%m-%Y') , ' s/d ',
                        DATE_FORMAT(DATE_ADD(DATE_ADD('$pasien_tgl_lahir', INTERVAL a.usia MONTH), INTERVAL a.max_toleransi DAY),'%d-%m-%Y')
                   ),0) AS range_usia,
                   CONCAT(COUNT(b.id), ' dari ' , COUNT(a.id))  AS imunisasi_status,
                     GROUP_CONCAT(CONCAT(b.jenis_imunisasi_id,'::',DATE_FORMAT(b.tgl,'%d-%m-%Y')) ORDER BY a.nama) AS imunisasi_dilakukan,
                     (COUNT(a.id) - COUNT(b.id)) AS jml_belum_dilakukan,
                     COUNT(b.id) AS jml_sudah_dilakukan
            FROM jenis_imunisasi a
            LEFT JOIN imunisasi b ON a.id = b.jenis_imunisasi_id AND b.pasien_id = $pasien_id
            GROUP BY usia");

        echo json_encode(
            array(
                'error'           => false,
                'message'         => 'Data berhasil diambil',
                'jadwalImunisasi' => $jadwal,
            )
        );
    }

    public function getRiwayatImunisasi(Request $request)
    {

        $pasien_id            = $request->pasien_id;
        $riwayatImunisasiList = DB::select(
            "SELECT a.id,
                    a.tgl,
                    b.nama AS jenis
             FROM imunisasi a
             LEFT JOIN jenis_imunisasi b ON a.jenis_imunisasi_id = b.id
             WHERE a.pasien_id = $pasien_id");

        echo json_encode(
            array(
                'error'                => false,
                'message'              => 'Data berhasil diambil',
                'riwayatImunisasiList' => $riwayatImunisasiList,
            )
        );
    }

    public function getRiwayatPemeriksaan(Request $request)
    {

        $pasien_id   = $request->pasien_id;
        $riwayatList = DB::select(
            "SELECT a.id,
                    a.tgl,
                    'Pemeriksaan' AS jenis
             FROM pemeriksaan a
             WHERE a.pasien_id = $pasien_id");

        echo json_encode(
            array(
                'error'                  => false,
                'message'                => 'Data berhasil diambil',
                'riwayatPemeriksaanList' => $riwayatList,
            )
        );
    }

    public function getDataBb(Request $request)
    {

        $pasien_id        = $request->pasien_id;
        $perkembanganList = DB::select(
            "SELECT DISTINCT TIMESTAMPDIFF(MONTH,b.tgl_lahir,NOW()) AS usia,
                    a.berat_badan AS nilai
             FROM antropometri a
             LEFT JOIN pasien b ON a.pasien_id = b.id
             WHERE a.pasien_id = $pasien_id AND a.berat_badan IS NOT NULL");

        echo json_encode(
            array(
                'error'            => false,
                'message'          => 'Data berhasil diambil',
                'perkembanganList' => $perkembanganList,
            )
        );
    }

    public function getDataTb(Request $request)
    {

        $pasien_id        = $request->pasien_id;
        $perkembanganList = DB::select(
            "SELECT DISTINCT TIMESTAMPDIFF(MONTH,b.tgl_lahir,NOW()) AS usia,
                    a.tinggi_badan AS nilai
             FROM antropometri a
             LEFT JOIN pasien b ON a.pasien_id = b.id
             WHERE a.pasien_id = $pasien_id AND a.tinggi_badan IS NOT NULL");

        echo json_encode(
            array(
                'error'            => false,
                'message'          => 'Data berhasil diambil',
                'perkembanganList' => $perkembanganList,
            )
        );
    }

    public function getDataLk(Request $request)
    {

        $pasien_id        = $request->pasien_id;
        $perkembanganList = DB::select(
            "SELECT DISTINCT TIMESTAMPDIFF(MONTH,b.tgl_lahir,NOW()) AS usia,
                    a.lingkar_kepala AS nilai
             FROM antropometri a
             LEFT JOIN pasien b ON a.pasien_id = b.id
             WHERE a.pasien_id = $pasien_id AND a.lingkar_kepala IS NOT NULL");

        echo json_encode(
            array(
                'error'            => false,
                'message'          => 'Data berhasil diambil',
                'perkembanganList' => $perkembanganList,
            )
        );
    }

}
