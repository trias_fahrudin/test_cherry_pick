<?php

namespace App\Http\Controllers;

use FCM;
use Illuminate\Support\Facades\DB;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class CronjobController extends Controller
{

    private function kirimNotifikasi($token, $title, $body)
    {

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setImage('')
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['message' => 'my_data']);

        $option       = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data         = $dataBuilder->build();

        // $token = "fwdM5FbPQa21HYFhxsJ8o9:APA91bHf8xn3L3W-xlbafSsWqZFvb7EznRShQfYtYlfPNw-3FtTH1ZZEvIRwSoxdC0tZ6j82uCAtzOnMBg6Dpm1roMyAdhFKZ-FOCVtkpCZGxQPwx7026C8VwpJ1Wx1p3xEZ_eORsoth";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

    }

    public function notifikasiAntrian()
    {

        // $antrian = DB::select('SELECT a.tgl,a.jenis,
        //                               DATEDIFF(tgl,NOW()) AS sisa_hari,
        //                               b.nama,c.token_id
        //                        FROM antrian_dokter a
        //                        LEFT JOIN pasien b ON a.pasien_id = b.id
        //                        LEFT JOIN ortu c ON b.ortu_id = c.id
        //                        WHERE (tgl >= NOW() - INTERVAL 1 DAY)  AND DATEDIFF(tgl,NOW()) <= 3 AND c.token_id IS NOT NULL');

        // foreach($antrian as $a){
        //     if($a->sisa_hari == 0){
        //         $body = "Jangan lupa ya ayah dan bunda ". $a->nama . ', hari ini adinda '. $a->nama . ' ada janji dengan dokter';
        //         $this->kirimNotifikasi($a->token_id,"SI PANJI",$body);
        //     }else{
        //         $body = "Jangan lupa ya ayah dan bunda ". $a->nama . ', ' . $a->sisa_hari . ' hari lagi adinda '. $a->nama . ' ada janji dengan dokter';
        //         $this->kirimNotifikasi($a->token_id,"SI PANJI",$body);
        //     }

        // }

        $pasien = DB::select("SELECT a.id,a.nama,a.tgl_lahir,b.token_id FROM pasien a
                              LEFT JOIN ortu b ON a.ortu_id = b.id");

        foreach ($pasien as $p) {
            $nama_pasien = $p->nama;
            $tgl_lahir   = $p->tgl_lahir;
            $pasien_id   = $p->id;
            $token_id    = $p->token_id;

            $notifikasi = DB::select("SELECT GROUP_CONCAT(a.nama) AS nama_imunisasi,
                                            a.usia,
                                            DATEDIFF(IFNULL(DATE_FORMAT(DATE_ADD('2019-08-21', INTERVAL a.usia MONTH),'%Y-%m-%d'),0) ,NOW()) AS sisa_hari,
                                            IFNULL(DATE_FORMAT(DATE_ADD('$tgl_lahir', INTERVAL a.usia MONTH),'%Y-%m-%d'),0) AS tanggal_imunisasi,
                                            COUNT(b.id) AS `status`
                                     FROM jenis_imunisasi a
                                     LEFT JOIN (SELECT a.id,
                                                    COUNT(a.id) AS jml,
                                                    a.jenis_imunisasi_id,
                                                    b.tgl_lahir
                                            FROM imunisasi a
                                            LEFT JOIN pasien b ON a.pasien_id = b.id
                                            WHERE b.id = $pasien_id
                                            GROUP BY a.id) b ON a.id = b.jenis_imunisasi_id
                                     GROUP BY a.usia
                                     HAVING (tanggal_imunisasi >= NOW() - INTERVAL 1 DAY)  AND DATEDIFF(tanggal_imunisasi,NOW()) <= 3
                                     ORDER BY a.usia ASC, COUNT(b.id) ASC");

            foreach ($notifikasi as $a) {
                if ($a->sisa_hari == 0) {
                    $body = "Jangan lupa ya ayah dan bunda " . $nama_pasien . ', hari ini adinda ' . $nama_pasien . ' waktunya melakukan imunisasi untuk anak usia ' . $a->usia . ' bulan';
                    $this->kirimNotifikasi($token_id,"SI PANJI",$body);
                    //echo $body;
                } else {
                    $body = "Jangan lupa ya ayah dan bunda " . $nama_pasien . ', ' . $a->sisa_hari . ' hari lagi adinda ' . $nama_pasien . ' waktunya melakukan imunisasi untuk anak usia ' . $a->usia . ' bulan';
                    $this->kirimNotifikasi($token_id,"SI PANJI",$body);
                    //echo $body;
                }
            }

        }

    }
}
