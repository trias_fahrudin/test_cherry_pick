<?php

namespace App\Http\Controllers;

use App\AntrianDokter;
use App\AntrianObat;
use App\Imunisasi;
use App\JenisImunisasi;
use App\Pemeriksaan;
use App\Pengguna;
use App\Traits\PerkembanganAnak;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class DokterController extends Controller
{

    use PerkembanganAnak;

    public function __construct()
    {

        $this->middleware('dokter');
    }

    public function index()
    {
        return redirect(URL::to('/dokter/antrian'));
        //return view('dokter.index');
    }

    /**/
    public function jenisImunisasi()
    {
        $ji = JenisImunisasi::all();
        return view('dokter.jenis_imunisasi.list', ['imunisasi' => $ji]);
    }

    public function jenisImunisasiHapus($id)
    {
        $ji = JenisImunisasi::find($id);
        $ji->delete();
        return redirect(URL::to('/dokter/jenis-imunisasi'));
    }

    public function jenisImunisasiEdit($id, Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'          => 'required',
                    'usia'          => 'required',
                    'max_toleransi' => 'required',
                ]);

                $ji                = JenisImunisasi::find($id);
                $ji->nama          = $request->nama;
                $ji->usia          = $request->usia;
                $ji->max_toleransi = $request->max_toleransi;
                $ji->keterangan    = $request->keterangan;

                $ji->save();
                return redirect(URL::to('/dokter/jenis-imunisasi'));

                break;
            case 'GET':

                $ji = JenisImunisasi::find($id);
                return view('dokter.jenis_imunisasi.edit', ['imunisasi' => $ji]);

                break;

            default:
                # code...
                break;
        }
    }

    public function jenisImunisasiTambah(Request $request)
    {

        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'          => 'required',
                    'usia'          => 'required',
                    'max_toleransi' => 'required',
                ]);

                JenisImunisasi::create([
                    'nama'          => $request->nama,
                    'usia'          => $request->usia,
                    'max_toleransi' => $request->max_toleransi,
                    'keterangan'    => $request->keterangan,
                ]);

                return redirect(URL::to('/dokter/jenis-imunisasi'));

                break;
            case 'GET':

                return view('dokter.jenis_imunisasi.tambah');

                break;

            default:
                # code...
                break;
        }

    }
    /**/

    public function profile(Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'  => 'required',
                    'telp'  => 'required',
                    'email' => 'email:rfc',

                ]);

                $pengguna        = Pengguna::find(Session::get('user_id'));
                $pengguna->nama  = $request->nama;
                $pengguna->telp  = $request->telp;
                $pengguna->email = $request->email;

                $pengguna->save();
                return redirect(URL::to('/dokter/profile'))->with('alert-success', 'Profile berhasil disimpan');
                // return $redirect->to('/admin/profile')->with('alert-danger', 'Password atau Email salah !')->send();

                break;
            case 'GET':

                $pengguna = Pengguna::find(Session::get('user_id'));
                return view('dokter.profile', ['pengguna' => $pengguna]);

                break;

            default:
                # code...
                break;
        }
    }

    public function daftarDataPasien()
    {
        $data = DB::select(
            "SELECT a.id,
                    a.nama AS nama,
                    CONCAT(a.tgl_lahir,' ( ', TIMESTAMPDIFF(MONTH,a.tgl_lahir, NOW()),' Bulan )') AS tgl_lahir,
                    CONCAT(b.nama_ayah,'',b.nama_ibu) AS ortu,
                    b.alamat
            FROM pasien AS a
            LEFT JOIN ortu AS b ON a.ortu_id = b.id
            ORDER BY a.nama ASC
        ");

        return view('dokter.pasien.list', ['data' => $data]);
    }

    public function riwayatKesehatanAjax($pasien_id)
    {

        $data_diri = DB::select("SELECT a.id,
                                        TIMESTAMPDIFF(MONTH,a.tgl_lahir,DATE(NOW())) AS usia,
                                        IF(a.jk = 'L','Laki-laki','Perempuan') AS jk,
                                        a.nama,
                                        IFNULL(a.foto,'no_foto.png') AS foto,
                                        b.alamat,
                                        CONCAT(a.tempat_lahir,', ',DATE_FORMAT(a.tgl_lahir,'%d %M %Y')) AS ttl
                                 FROM pasien a
                                 LEFT JOIN ortu b ON a.ortu_id = b.id
                                 WHERE a.id = $pasien_id")[0];
        // var_dump($data_diri);

        $riwayat = DB::select(
            "(SELECT a.id,DATE(a.tgl) AS tgl,
                   'Imunisasi' AS pelayanan,
                   '-' AS `diagnosa`,
                   IFNULL(a.resep,'-') AS `resep`,
                   IFNULL(CONCAT('BB:',c.berat_badan,' - TB:' , c.tinggi_badan,' - LK:', c.lingkar_kepala),'-') AS `antropometri`,
                   CONCAT('Imunisasi - ',b.nama) AS `tindakan`
            FROM imunisasi a
            LEFT JOIN  jenis_imunisasi b ON a.jenis_imunisasi_id = b.id
            LEFT JOIN antropometri c ON DATE(a.tgl) = DATE(c.tgl)
            WHERE a.pasien_id = $pasien_id)
            UNION
            (SELECT a.id,DATE(a.tgl) AS tgl,
                    'Pemeriksaan' AS pelayanan,
                      a.diagnosa AS `diagnosa`,
                      IFNULL(resep,'-') AS `resep`,
                      IFNULL(CONCAT('BB:',b.berat_badan,' - TB:' , b.tinggi_badan,' - LK:', b.lingkar_kepala),'-') AS `antropometri`,
                      a.tindakan
            FROM pemeriksaan a
            LEFT JOIN antropometri b ON DATE(a.tgl) = DATE(b.tgl)
            WHERE a.pasien_id = $pasien_id)");

        $returnAjax = view('dokter.pasien.riwayatKesehatanAjax', [
            'riwayat'   => $riwayat,
            'data_diri' => $data_diri,
            'bb_u'      => $this->bb_u($pasien_id, $data_diri->jk),
            'tb_u'      => $this->tb_u($pasien_id, $data_diri->jk),
            'lk_u'      => $this->lk_u($pasien_id, $data_diri->jk),
        ]
        )->render();

        return response()->json(array('success' => true, 'html' => $returnAjax));
    }

    public function riwayatPemeriksaanAjax($pasien_id)
    {
        $riwayat = DB::select(
            "SELECT DATE(a.tgl) AS tgl,
                    TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) AS usia,
                    a.anamnesa,
                    a.diagnosa,
                    a.tindakan,
                    a.resep
             FROM pemeriksaan a
             LEFT JOIN pasien b ON a.pasien_id = b.id
             WHERE a.pasien_id = $pasien_id
             ORDER BY a.tgl DESC");

        $returnAjax = view('dokter.pemeriksaan.riwayatAjax', ['riwayat' => $riwayat])->render();
        return response()->json(array('success' => true, 'html' => $returnAjax));
    }

    public function riwayatImunisasiAjax($pasien_id)
    {
        $riwayat = DB::select(
            "SELECT DATE(a.tgl) AS tgl,
                     TIMESTAMPDIFF(MONTH,c.tgl_lahir,a.tgl) AS usia,
                     b.nama AS jenis_imunisasi,
                     a.keterangan,
                     a.resep
              FROM imunisasi a
              LEFT JOIN jenis_imunisasi b ON a.jenis_imunisasi_id = b.id
              LEFT JOIN pasien c ON a.pasien_id = c.id
              WHERE a.pasien_id = $pasien_id
              ORDER BY a.tgl DESC");

        $returnAjax = view('dokter.imunisasi.riwayatAjax', ['riwayat' => $riwayat])->render();
        return response()->json(array('success' => true, 'html' => $returnAjax));
    }

    public function riwayatAntropometriAjax($pasien_id)
    {
        $riwayat = DB::select(
            "SELECT DATE(a.tgl) AS tgl,
                     TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) AS usia,
                     a.berat_badan AS bb,
                     a.tinggi_badan AS tb,
                     a.lingkar_kepala AS lk
              FROM antropometri a
              LEFT JOIN pasien b ON a.pasien_id = b.id
              WHERE a.pasien_id = $pasien_id
              ORDER BY a.tgl DESC");

        $returnAjax = view('dokter.antropometri.riwayatAjax', ['riwayat' => $riwayat])->render();
        return response()->json(array('success' => true, 'html' => $returnAjax));
    }

    public function pemeriksaanSimpan($pasien_id, Request $request)
    {
        Pemeriksaan::create([
            'tgl'       => Carbon::now(),
            'pasien_id' => $pasien_id,
            'anamnesa'  => $request->anamnesa,
            'diagnosa'  => $request->diagnosa,
            'tindakan'  => $request->tindakan,
            'resep'     => $request->resep,

        ]);

        if (trim($request->resep) !== '') {
            //tambahan ke antrian obat
            AntrianObat::create([
                'tgl'       => Carbon::now(),
                'jenis'     => 'pemeriksaan',
                'pasien_id' => $pasien_id,
                'status'    => 'antri',
            ]);
        }

        DB::statement("UPDATE antrian_dokter SET status='selesai' WHERE pasien_id = $pasien_id AND DATE(tgl) = DATE(now())");

        return redirect(URL::to('/dokter/antrian'));
    }

    public function imunisasiSimpan($pasien_id, Request $request)
    {
        Imunisasi::create([
            'tgl'                => Carbon::now(),
            'pasien_id'          => $pasien_id,
            'jenis_imunisasi_id' => $request->jenis_imunisasi_id,
            'keterangan'         => $request->keterangan,
            'resep'              => $request->resep,

        ]);

        if (trim($request->resep) !== '') {
            //tambahan ke antrian obat
            AntrianObat::create([
                'tgl'       => Carbon::now(),
                'jenis'     => 'imunisasi',
                'pasien_id' => $pasien_id,
                'status'    => 'antri',
            ]);
        }

        if (trim($request->jadwal_imunisai_selanjutnya) !== '') {
            AntrianDokter::create([
                'jenis'     => 'imunisasi',
                'tgl'       => $request->jadwal_imunisai_selanjutnya . ' ' . Carbon::now()->format('h:i:s'),
                'pasien_id' => $pasien_id,
                'status'    => 'antri',

            ]);
        }

        DB::statement("UPDATE antrian_dokter SET status='selesai' WHERE pasien_id = $pasien_id AND DATE(tgl) = DATE(now())");

        return redirect(URL::to('/dokter/antrian'));
    }

    public function formImunisasiAjax($pasien_id)
    {
        $riwayat = DB::select(
            "SELECT a.id,CONCAT(a.nama,' - (', a.usia , ' Bulan)') AS nama
             FROM jenis_imunisasi a
             WHERE a.id NOT IN (SELECT a.jenis_imunisasi_id
                                FROM imunisasi a
                                WHERE a.pasien_id = " . $pasien_id . ")
             ORDER BY a.id ASC");

        $returnAjax = view('dokter.imunisasi.formRiwayatAjax', ['jenis_imunisasi' => $riwayat])->render();
        return response()->json(array('success' => true, 'html' => $returnAjax));

    }

    public function antrianAjax()
    {

        $antrian = DB::select(
            "SELECT a.id,
                    a.pasien_id,
                    b.nama AS nama,
                    CONCAT(b.tgl_lahir,' ( ', TIMESTAMPDIFF(MONTH,b.tgl_lahir, NOW()),' Bulan )') AS tgl_lahir,
                    CONCAT(c.nama_ayah,'/',c.nama_ibu) AS ortu,
                    c.alamat,
                    COUNT(d.id) AS pemeriksaan_antropometri,
                    a.jenis,
                    a.status
            FROM antrian_dokter AS a
            LEFT JOIN pasien AS b ON a.pasien_id = b.id
            LEFT JOIN ortu AS c ON b.ortu_id = c.id
            LEFT JOIN antropometri AS d ON b.id = d.pasien_id AND DATE(d.tgl) = DATE(a.tgl)
            WHERE DATE(a.tgl) = DATE(now()) AND a.status IN ('diperiksa')
            GROUP BY a.id
            HAVING a.id IS NOT NULL
            ORDER BY a.tgl ASC");

        $imunisasi_hari_ini = DB::select(
            "SELECT b.nama AS jenis,
                    COUNT(a.id) AS jml
            FROM imunisasi a
            LEFT JOIN jenis_imunisasi b ON a.jenis_imunisasi_id = b.id
            WHERE DATE(a.tgl) = DATE(NOW())
            GROUP BY b.id"
        );

        $imunisasi       = DB::select("SELECT CONCAT(COUNT(id),' Pasien') AS jml FROM imunisasi WHERE DATE(tgl) = DATE(NOW())")[0];
        $antrian_aktif   = DB::select("SELECT CONCAT(COUNT(id),' Pasien') AS jml FROM antrian_dokter WHERE DATE(tgl) = DATE(NOW()) AND `status` = 'antri'")[0];
        $antrian_selesai = DB::select("SELECT CONCAT(COUNT(id),' Pasien') AS jml FROM antrian_dokter WHERE DATE(tgl) = DATE(NOW()) AND `status` IN ('selesai','diperiksa')")[0];

        $returnAjax          = view('dokter.antrian.listAjax', ['antrian' => $antrian])->render();
        $returnImunisasiAjax = view('dokter.antrian.listImunisasiAjax', ['imunisasi' => $imunisasi_hari_ini])->render();

        return response()->json(
            array(
                'success'        => true,
                'jml_imunisasi'  => $imunisasi->jml,
                'jml_antrian'    => $antrian_aktif->jml,
                'jml_ditangani'  => $antrian_selesai->jml,
                'html'           => $returnAjax,
                'imunisasi_html' => $returnImunisasiAjax,
            )
        );
    }

    public function antrian()
    {
        // var_dump(Carbon::now()->format('h:i:s'));
        $antrian = DB::select(
            "SELECT a.id,
                    a.pasien_id,
                    b.nama AS nama,
                    CONCAT(b.tgl_lahir,' ( ', TIMESTAMPDIFF(MONTH,b.tgl_lahir, NOW()),' Bulan )') AS tgl_lahir,
                    CONCAT(c.nama_ayah,'/',c.nama_ibu) AS ortu,
                    c.alamat,
                    COUNT(d.id) AS pemeriksaan_antropometri,
                    a.jenis,
                    a.status
            FROM antrian_dokter AS a
            LEFT JOIN pasien AS b ON a.pasien_id = b.id
            LEFT JOIN ortu AS c ON b.ortu_id = c.id
            LEFT JOIN antropometri AS d ON b.id = d.pasien_id AND DATE(d.tgl) = DATE(a.tgl)
            WHERE DATE(a.tgl) = DATE(now()) AND a.status IN ('diperiksa')
            GROUP BY a.id
            HAVING a.id IS NOT NULL
            ORDER BY a.tgl ASC
        ");

        return view('dokter.antrian.list', ['antrian' => $antrian]);
    }

}
