<?php

namespace App\Http\Controllers;

use App\AntrianDokter;
use App\Antropometri;
use App\Ortu;
use App\Pasien;
use App\Pengguna;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use App\Traits\HelperFunction;

class ResepsionisController extends Controller
{

    use HelperFunction;
    
    public function __construct()
    {

        $this->middleware('resepsionis');
    }

    public function index()
    {
        return redirect(URL::to('/resepsionis/antrian'));
        //return view('resepsionis.index');
    }

    public function profile(Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'  => 'required',
                    'telp'  => 'required',
                    'email' => 'email:rfc',

                ]);

                $pengguna        = Pengguna::find(Session::get('user_id'));
                $pengguna->nama  = $request->nama;
                $pengguna->telp  = $request->telp;
                $pengguna->email = $request->email;

                $pengguna->save();
                return redirect(URL::to('/resepsionis/profile'))->with('alert-success', 'Profile berhasil disimpan');
                // return $redirect->to('/admin/profile')->with('alert-danger', 'Password atau Email salah !')->send();

                break;
            case 'GET':

                $pengguna = Pengguna::find(Session::get('user_id'));
                return view('resepsionis.profile', ['pengguna' => $pengguna]);

                break;

            default:
                # code...
                break;
        }
    }

    public function antrianSetDiperiksa($antrian_id)
    {
        DB::table('antrian_dokter')
            ->where('id', $antrian_id)
            ->update(['status' => 'diperiksa']);

        return redirect(URL::to('/resepsionis/antrian'));
    }

    public function antrianSetDilewati($antrian_id)
    {
        DB::table('antrian_dokter')
            ->where('id', $antrian_id)
            ->update(['status' => 'lewati']);

        return redirect(URL::to('/resepsionis/antrian'));
    }

    public function masukkanKembaliPasienDilewati($antrian_id)
    {
        DB::table('antrian_dokter')
            ->where('id', $antrian_id)
            ->update(['status' => 'antri']);

        return redirect(URL::to('/resepsionis/antrian'));
    }

    public function dataPasienJson()
    {
        return DataTables::of(Pasien::all())->make(true);
    }

    public function dataPasienDilewatiJson()
    {
        //AntrianDokter::where('status','=','lewati')
        /*
        { data: 'nama', name: 'nama' },
        { data: 'jk', name: 'jk' },
        { data: 'tempat_lahir', name: 'tempat_lahir' },
        { data: 'tgl_lahir', name: 'tgl_lahir' },
        { data: 'id', name:'id'},

         */
        return DataTables::of(DB::select(
            "SELECT b.nama,b.jk,b.tempat_lahir,b.tgl_lahir,a.id
             FROM antrian_dokter a
             LEFT JOIN pasien b ON a.pasien_id = b.id
             WHERE a.status = 'lewati'"))->make(true);
    }

    public function antrianTambahData($pasien_id, $jenis, $tgl_antrian)
    {
        AntrianDokter::create([
            'jenis'     => ($jenis === '000' ? 'pemeriksaan' : 'imunisasi'),
            'tgl'       => $tgl_antrian . ' ' . Carbon::now()->format('h:i:s'),
            'pasien_id' => $pasien_id,
            'status'    => 'antri',

        ]);

        // DB::statement('CALL update_antrian()');
        $this->update_antrian();
        return redirect(URL::to('/resepsionis/antrian'));
    }

    public function antrianAjax(Request $request)
    {

        // $antrian = DB::table('antrian_dokter AS a')
        //     ->select(DB::raw("a.id,
        //                       a.pasien_id,
        //                       b.nama AS nama,
        //                       CONCAT(b.tgl_lahir,' ( ',timestampdiff(MONTH,b.tgl_lahir,now()),' Bulan )') AS tgl_lahir,
        //                       CONCAT(c.nama_ayah,'/',c.nama_ibu) AS ortu,
        //                       c.alamat,
        //                       COUNT(d.id) AS pemeriksaan_antropometri,
        //                       a.jenis,a.status"))
        //     ->leftjoin('pasien AS b', 'a.pasien_id', '=', 'b.id')
        //     ->leftjoin('ortu AS c', 'b.ortu_id', '=', 'c.id')
        //     ->leftjoin('antropometri AS d','b.id','=',DB::raw('d.pasien_id AND DATE(d.tgl) = DATE(a.tgl)'))
        //     ->whereRaw("DATE(a.tgl) = '" . date('Y-m-d') . "' AND a.status NOT IN ('lewati','selesai')")
        //     ->orderBy('a.tgl', 'ASC')
        //     ->get();
        //

        $tgl_antrian = $request->tgl_antrian;

        $antrian = DB::select(
            "SELECT a.id,
                  CONCAT('PASIEN:',a.pasien_id) AS kode,
                  a.antrian_ke,
                  a.pasien_id,
                  b.nama AS nama,
                  CONCAT(b.tgl_lahir,' ( ', TIMESTAMPDIFF(MONTH,b.tgl_lahir, NOW()),' Bulan )') AS tgl_lahir,
                  CONCAT(c.nama_ayah,'/',c.nama_ibu) AS ortu,
                  c.alamat,
                  COUNT(d.id) AS pemeriksaan_antropometri,
                  a.jenis,
                  a.status
            FROM antrian_dokter AS a
            LEFT JOIN pasien AS b ON a.pasien_id = b.id
            LEFT JOIN ortu AS c ON b.ortu_id = c.id
            LEFT JOIN antropometri AS d ON b.id = d.pasien_id AND DATE(d.tgl) = DATE(a.tgl)
            WHERE DATE(a.tgl) = '" . $tgl_antrian . "' AND a.status NOT IN ('lewati','selesai')
            GROUP BY a.id
            HAVING a.id IS NOT NULL
            ORDER BY a.tgl ASC");

        $returnAjax = view('resepsionis.antrian.listAjax', ['antrian' => $antrian])->render();
        return response()->json(array('success' => true, 'html' => $returnAjax));
    }

    public function antrian()
    {
        $antrian = DB::select(
            "SELECT 
                  CONCAT('PASIEN:',a.pasien_id) AS kode,
                  a.id,
                  a.antrian_ke,
                  a.pasien_id,
                  b.nama AS nama,
                  CONCAT(b.tgl_lahir,' ( ', TIMESTAMPDIFF(MONTH,b.tgl_lahir, NOW()),' Bulan )') AS tgl_lahir,
                  CONCAT(c.nama_ayah,'/',c.nama_ibu) AS ortu,
                  c.alamat,
                  COUNT(d.id) AS pemeriksaan_antropometri,
                  a.jenis,
                  a.status
            FROM antrian_dokter AS a
            LEFT JOIN pasien AS b ON a.pasien_id = b.id
            LEFT JOIN ortu AS c ON b.ortu_id = c.id
            LEFT JOIN antropometri AS d ON b.id = d.pasien_id AND DATE(d.tgl) = DATE(a.tgl)
            WHERE DATE(a.tgl) = DATE(now()) AND a.status NOT IN ('lewati','selesai')
            GROUP BY a.id
            HAVING a.id IS NOT NULL
            ORDER BY a.tgl ASC");

        return view('resepsionis.antrian.list', ['antrian' => $antrian]);
    }

    public function pasien($ortu_id)
    {
        $pasien = Pasien::where('ortu_id', $ortu_id)->get();
        return view('resepsionis.pasien.list',
            [
                'ortu_id' => $ortu_id,
                'pasien'  => $pasien,
            ]
        );
    }

    public function simpanAntropometri(Request $request)
    {
        Antropometri::create([
            'tgl'            => Carbon::now(),
            'pasien_id'      => $request->pasien_id,
            'berat_badan'    => $request->berat_badan,
            'tinggi_badan'   => $request->tinggi_badan,
            'lingkar_kepala' => $request->lingkar_kepala,
        ]);

        return redirect(URL::to('/resepsionis/antrian'));

    }

    public function pasienTambah($ortu_id, Request $request)
    {

        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'         => 'required',
                    'jk'           => 'required',
                    'tempat_lahir' => 'required',
                    'tgl_lahir'    => 'required',
                ]);

                Pasien::create([
                    'ortu_id'      => $ortu_id,
                    'nama'         => $request->nama,
                    'jk'           => $request->jk,
                    'tempat_lahir' => $request->tempat_lahir,
                    'tgl_lahir'    => $request->tgl_lahir,
                ]);

                return redirect(URL::to('/resepsionis/pasien/' . $ortu_id));

                break;
            case 'GET':

                return view('resepsionis.pasien.tambah', ['ortu_id' => $ortu_id]);

                break;

            default:
                # code...
                break;
        }

    }

    public function pasienHapus($ortu_id, $id)
    {
        $pasien = Pasien::find($id);
        $pasien->delete();
        return redirect(URL::to('/resepsionis/pasien/' . $ortu_id));
    }

    public function pasienEdit($ortu_id, $id, Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama'         => 'required',
                    'jk'           => 'required',
                    'tempat_lahir' => 'required',
                    'tgl_lahir'    => 'required',
                    'foto'         => 'file|image|mimes:jpeg,png,jpg|max:2048',
                ]);

                $pasien               = Pasien::find($id);
                $pasien->nama         = $request->nama;
                $pasien->jk           = $request->jk;
                $pasien->tempat_lahir = $request->tempat_lahir;
                $pasien->tgl_lahir    = $request->tgl_lahir;

                if ($_FILES['foto']['size'] != 0) {
                    // menyimpan data file yang diupload ke variabel $file
                    $foto = $request->file('foto');

                    $nama_file = time() . "_" . $foto->getClientOriginalName();

                    // isi dengan nama folder tempat kemana file diupload
                    $tujuan_upload = 'data_file';
                    $foto->move($tujuan_upload, $nama_file);

                    $pasien->foto = $nama_file;

                }

                $pasien->save();
                return redirect(URL::to('/resepsionis/pasien/' . $ortu_id));

                break;
            case 'GET':

                $pasien = Pasien::find($id);
                return view('resepsionis.pasien.edit', ['pasien' => $pasien, 'pasien_id' => $id, 'ortu_id' => $ortu_id]);

                break;

            default:
                # code...
                break;
        }
    }

    ######

    public function ortu()
    {
        $ortu = Ortu::all();
        return view('resepsionis.ortu.list', ['ortu' => $ortu]);
    }

    public function ortuHapus($id)
    {
        $ortu = Ortu::find($id);
        $ortu->delete();
        return redirect(URL::to('/resepsionis/ortu'));
    }

    public function ortuEdit($id, Request $request)
    {
        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'nama_ayah' => 'required',
                    'nama_ibu'  => 'required',
                    'alamat'    => 'required',
                    'prov_id'   => 'required',
                    'kab_id'    => 'required',
                    'kec_id'    => 'required',
                    'email'     => 'email',
                ]);

                $ortu            = Ortu::find($id);
                $ortu->nama_ayah = $request->nama_ayah;
                $ortu->nama_ibu  = $request->nama_ibu;
                $ortu->alamat    = $request->alamat;
                $ortu->telp      = $request->telp;
                $ortu->prov_id   = $request->prov_id;
                $ortu->kab_id    = $request->kab_id;
                $ortu->kec_id    = $request->kec_id;
                $ortu->email     = $request->email;

                $ortu->save();
                return redirect(URL::to('/resepsionis/ortu'));

                break;
            case 'GET':

                $ortu = Ortu::find($id);
                $prov = DB::select("SELECT id,nama FROM wil_prov");
                return view('resepsionis.ortu.edit', ['ortu' => $ortu, 'prov' => $prov]);

                break;

            default:
                # code...
                break;
        }
    }

    public function ortuTambah(Request $request)
    {

        switch ($request->method()) {
            case 'POST':

                $this->validate($request, [
                    'username'  => 'required',
                    'password'  => 'required',
                    'nama_ayah' => 'required',
                    'nama_ibu'  => 'required',
                    'alamat'    => 'required',
                    'prov_id'   => 'required',
                    'kab_id'    => 'required',
                    'kec_id'    => 'required',
                    'email'     => 'email',
                ]);

                Ortu::create([
                    'username'  => $request->username,
                    'password'  => md5($request->password),
                    'nama_ayah' => $request->nama_ayah,
                    'nama_ibu'  => $request->nama_ibu,
                    'alamat'    => $request->alamat,
                    'telp'      => $request->telp,
                    'prov_id'   => $request->prov_id,
                    'kab_id'    => $request->kab_id,
                    'kec_id'    => $request->kec_id,
                    'email'     => $request->email,
                ]);

                return redirect(URL::to('/resepsionis/ortu'));

                break;
            case 'GET':

                $prov = DB::select("SELECT id,nama FROM wil_prov");
                return view('resepsionis.ortu.tambah', ['prov' => $prov]);

                break;

            default:
                # code...
                break;
        }

    }

    //##################

    public function getNamaPasien(Request $request){
        header('content-type: application/json');
        
        $pasien_id = $request->pasien_id;
        $pasien = Pasien::find($pasien_id);

        echo $pasien->nama;

    }

}
