<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisImunisasi extends Model
{
    //
    protected $table    = 'jenis_imunisasi';
    protected $fillable = [
        'nama',
        'usia',
        'max_toleransi',
        'keterangan'
    ];
}
