<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait HelperFunction
{
    public function update_antrian()
    {

        $q_tgl = DB::select("SELECT DISTINCT DATE(tgl) AS tgl FROM antrian_dokter");

        foreach ($q_tgl as $q) {

            $q_row = DB::select("SELECT id FROM antrian_dokter WHERE DATE(tgl) = ? ORDER BY id ASC", [$q->tgl]);

            $i = 1;
            foreach ($q_row as $row) {

                DB::table('antrian_dokter')
                    ->where('id', $row->id)
                    ->update(['antrian_ke' => $i]);

                $i++;

            }

        }

    }
}
