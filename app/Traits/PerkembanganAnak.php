<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait PerkembanganAnak
{

    public function bb_u($pasien_id, $pasien_jk)
    {

        $anak = DB::select("
                    SELECT b.tgl_lahir,
                           TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) AS usia_saat_pemeriksaan,
                           a.berat_badan,
                           a.tinggi_badan,
                           a.lingkar_kepala
                    FROM antropometri a
                    LEFT JOIN pasien b ON a.pasien_id = b.id
                    WHERE a.pasien_id = $pasien_id");

        // var_dump($anak);

        $array_anak = array();
        foreach ($anak as $key) {
            $array_anak[$key->usia_saat_pemeriksaan] = $key->berat_badan;
        }

        $bb_u = DB::select("SELECT * FROM standart_bb_u WHERE jk = '$pasien_jk'");

        $min_3_sd  = array();
        $min_2_sd  = array();
        $min_1_sd  = array();
        $median    = array();
        $plus_1_sd = array();
        $plus_2_sd = array();
        $plus_3_sd = array();

        foreach ($bb_u as $row) {
            $min_3_sd[$row->umur_bulan]  = $row->min_3_sd;
            $min_2_sd[$row->umur_bulan]  = $row->min_2_sd;
            $min_1_sd[$row->umur_bulan]  = $row->min_1_sd;
            $median[$row->umur_bulan]    = $row->median;
            $plus_1_sd[$row->umur_bulan] = $row->plus_1_sd;
            $plus_2_sd[$row->umur_bulan] = $row->plus_2_sd;
            $plus_3_sd[$row->umur_bulan] = $row->plus_3_sd;
        }

        for ($i = 0; $i <= 60; $i++) {
            $min_3_sd[$i]  = !isset($min_3_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_3_sd[$i] . ']';
            $min_2_sd[$i]  = !isset($min_2_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_2_sd[$i] . ']';
            $min_1_sd[$i]  = !isset($min_1_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_1_sd[$i] . ']';
            $median[$i]    = !isset($median[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $median[$i] . ']';
            $plus_1_sd[$i] = !isset($plus_1_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_1_sd[$i] . ']';
            $plus_2_sd[$i] = !isset($plus_2_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_2_sd[$i] . ']';
            $plus_3_sd[$i] = !isset($plus_3_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_3_sd[$i] . ']';

            $array_anak[$i] = !isset($array_anak[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $array_anak[$i] . ']';

        }

        ksort($min_3_sd);
        ksort($min_2_sd);
        ksort($min_1_sd);
        ksort($median);
        ksort($plus_1_sd);
        ksort($plus_2_sd);
        ksort($plus_3_sd);
        ksort($array_anak);

        $array_bb_u = array();

        $array_bb_u['min_3_sd']   = implode(',', $min_3_sd);
        $array_bb_u['min_2_sd']   = implode(',', $min_2_sd);
        $array_bb_u['min_1_sd']   = implode(',', $min_1_sd);
        $array_bb_u['median']     = implode(',', $median);
        $array_bb_u['plus_1_sd']  = implode(',', $plus_1_sd);
        $array_bb_u['plus_2_sd']  = implode(',', $plus_2_sd);
        $array_bb_u['plus_3_sd']  = implode(',', $plus_3_sd);
        $array_bb_u['array_anak'] = implode(',', $array_anak);

        $returnAjax = view('perkembangan.bb_u',
            [
                'bb_u_min_3_sd'   => $array_bb_u['min_3_sd'],
                'bb_u_min_2_sd'   => $array_bb_u['min_2_sd'],
                'bb_u_min_1_sd'   => $array_bb_u['min_1_sd'],
                'bb_u_median'     => $array_bb_u['median'],
                'bb_u_plus_1_sd'  => $array_bb_u['plus_1_sd'],
                'bb_u_plus_2_sd'  => $array_bb_u['plus_2_sd'],
                'bb_u_plus_3_sd'  => $array_bb_u['plus_3_sd'],
                'bb_u_array_anak' => $array_bb_u['array_anak'],
            ])->render();

        return $returnAjax;

    }

    public function tb_u($pasien_id, $pasien_jk)
    {

        $anak = DB::select("
                    SELECT b.tgl_lahir,
                           TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) AS usia_saat_pemeriksaan,
                           a.berat_badan,
                           a.tinggi_badan,
                           a.lingkar_kepala
                    FROM antropometri a
                    LEFT JOIN pasien b ON a.pasien_id = b.id
                    WHERE a.pasien_id = $pasien_id");

        // var_dump($anak);

        $array_anak = array();
        foreach ($anak as $key) {
            $array_anak[$key->usia_saat_pemeriksaan] = $key->tinggi_badan;
        }

        $min_3_sd  = array();
        $min_2_sd  = array();
        $min_1_sd  = array();
        $median    = array();
        $plus_1_sd = array();
        $plus_2_sd = array();
        $plus_3_sd = array();

        //data untuk panjang badan (usia <= 24 bulan)
        $pb_u = DB::select("SELECT * FROM standart_pb_u WHERE jk = '$pasien_jk'");

        foreach ($pb_u as $row) {
            $min_3_sd[$row->umur_bulan]  = $row->min_3_sd;
            $min_2_sd[$row->umur_bulan]  = $row->min_2_sd;
            $min_1_sd[$row->umur_bulan]  = $row->min_1_sd;
            $median[$row->umur_bulan]    = $row->median;
            $plus_1_sd[$row->umur_bulan] = $row->plus_1_sd;
            $plus_2_sd[$row->umur_bulan] = $row->plus_2_sd;
            $plus_3_sd[$row->umur_bulan] = $row->plus_3_sd;
        }

        //data untuk tinggi badan (usia > 24 bulan)
        $tb_u = DB::select("SELECT * FROM standart_tb_u WHERE jk = '$pasien_jk'");

        foreach ($tb_u as $row) {
            $min_3_sd[$row->umur_bulan]  = $row->min_3_sd;
            $min_2_sd[$row->umur_bulan]  = $row->min_2_sd;
            $min_1_sd[$row->umur_bulan]  = $row->min_1_sd;
            $median[$row->umur_bulan]    = $row->median;
            $plus_1_sd[$row->umur_bulan] = $row->plus_1_sd;
            $plus_2_sd[$row->umur_bulan] = $row->plus_2_sd;
            $plus_3_sd[$row->umur_bulan] = $row->plus_3_sd;
        }

        for ($i = 0; $i <= 60; $i++) {
            $min_3_sd[$i]  = !isset($min_3_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_3_sd[$i] . ']';
            $min_2_sd[$i]  = !isset($min_2_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_2_sd[$i] . ']';
            $min_1_sd[$i]  = !isset($min_1_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_1_sd[$i] . ']';
            $median[$i]    = !isset($median[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $median[$i] . ']';
            $plus_1_sd[$i] = !isset($plus_1_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_1_sd[$i] . ']';
            $plus_2_sd[$i] = !isset($plus_2_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_2_sd[$i] . ']';
            $plus_3_sd[$i] = !isset($plus_3_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_3_sd[$i] . ']';

            $array_anak[$i] = !isset($array_anak[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $array_anak[$i] . ']';

        }

        ksort($min_3_sd);
        ksort($min_2_sd);
        ksort($min_1_sd);
        ksort($median);
        ksort($plus_1_sd);
        ksort($plus_2_sd);
        ksort($plus_3_sd);
        ksort($array_anak);

        $array_tb_u = array();

        $array_tb_u['min_3_sd']   = implode(',', $min_3_sd);
        $array_tb_u['min_2_sd']   = implode(',', $min_2_sd);
        $array_tb_u['min_1_sd']   = implode(',', $min_1_sd);
        $array_tb_u['median']     = implode(',', $median);
        $array_tb_u['plus_1_sd']  = implode(',', $plus_1_sd);
        $array_tb_u['plus_2_sd']  = implode(',', $plus_2_sd);
        $array_tb_u['plus_3_sd']  = implode(',', $plus_3_sd);
        $array_tb_u['array_anak'] = implode(',', $array_anak);

        $returnAjax = view('perkembangan.tb_u',
            [
                'tb_u_min_3_sd'   => $array_tb_u['min_3_sd'],
                'tb_u_min_2_sd'   => $array_tb_u['min_2_sd'],
                'tb_u_min_1_sd'   => $array_tb_u['min_1_sd'],
                'tb_u_median'     => $array_tb_u['median'],
                'tb_u_plus_1_sd'  => $array_tb_u['plus_1_sd'],
                'tb_u_plus_2_sd'  => $array_tb_u['plus_2_sd'],
                'tb_u_plus_3_sd'  => $array_tb_u['plus_3_sd'],
                'tb_u_array_anak' => $array_tb_u['array_anak'],
            ])->render();

        return $returnAjax;

    }

    public function lk_u($pasien_id, $pasien_jk)
    {

        $anak = DB::select("
                    SELECT b.tgl_lahir,
                           TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) AS usia_saat_pemeriksaan,
                           a.berat_badan,
                           a.tinggi_badan,
                           a.lingkar_kepala
                    FROM antropometri a
                    LEFT JOIN pasien b ON a.pasien_id = b.id
                    WHERE a.pasien_id = $pasien_id");

        // var_dump($anak);

        $array_anak = array();
        foreach ($anak as $key) {
            $array_anak[$key->usia_saat_pemeriksaan] = $key->lingkar_kepala;
        }

        $lk_u = DB::select("SELECT * FROM standart_lk_u WHERE jk = '$pasien_jk'");

        $min_3_sd  = array();
        $min_2_sd  = array();
        $min_1_sd  = array();
        $median    = array();
        $plus_1_sd = array();
        $plus_2_sd = array();
        $plus_3_sd = array();

        foreach ($lk_u as $row) {
            $min_3_sd[$row->umur_bulan]  = $row->min_3_sd;
            $min_2_sd[$row->umur_bulan]  = $row->min_2_sd;
            $min_1_sd[$row->umur_bulan]  = $row->min_1_sd;
            $median[$row->umur_bulan]    = $row->median;
            $plus_1_sd[$row->umur_bulan] = $row->plus_1_sd;
            $plus_2_sd[$row->umur_bulan] = $row->plus_2_sd;
            $plus_3_sd[$row->umur_bulan] = $row->plus_3_sd;
        }

        for ($i = 0; $i <= 60; $i++) {
            $min_3_sd[$i]  = !isset($min_3_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_3_sd[$i] . ']';
            $min_2_sd[$i]  = !isset($min_2_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_2_sd[$i] . ']';
            $min_1_sd[$i]  = !isset($min_1_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $min_1_sd[$i] . ']';
            $median[$i]    = !isset($median[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $median[$i] . ']';
            $plus_1_sd[$i] = !isset($plus_1_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_1_sd[$i] . ']';
            $plus_2_sd[$i] = !isset($plus_2_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_2_sd[$i] . ']';
            $plus_3_sd[$i] = !isset($plus_3_sd[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $plus_3_sd[$i] . ']';

            $array_anak[$i] = !isset($array_anak[$i]) ? '[' . $i . ',0]' : '[' . $i . ',' . $array_anak[$i] . ']';

        }

        ksort($min_3_sd);
        ksort($min_2_sd);
        ksort($min_1_sd);
        ksort($median);
        ksort($plus_1_sd);
        ksort($plus_2_sd);
        ksort($plus_3_sd);
        ksort($array_anak);

        $array_lk_u = array();

        $array_lk_u['min_3_sd']   = implode(',', $min_3_sd);
        $array_lk_u['min_2_sd']   = implode(',', $min_2_sd);
        $array_lk_u['min_1_sd']   = implode(',', $min_1_sd);
        $array_lk_u['median']     = implode(',', $median);
        $array_lk_u['plus_1_sd']  = implode(',', $plus_1_sd);
        $array_lk_u['plus_2_sd']  = implode(',', $plus_2_sd);
        $array_lk_u['plus_3_sd']  = implode(',', $plus_3_sd);
        $array_lk_u['array_anak'] = implode(',', $array_anak);

        $returnAjax = view('perkembangan.lk_u',
            [
                'lk_u_min_3_sd'   => $array_lk_u['min_3_sd'],
                'lk_u_min_2_sd'   => $array_lk_u['min_2_sd'],
                'lk_u_min_1_sd'   => $array_lk_u['min_1_sd'],
                'lk_u_median'     => $array_lk_u['median'],
                'lk_u_plus_1_sd'  => $array_lk_u['plus_1_sd'],
                'lk_u_plus_2_sd'  => $array_lk_u['plus_2_sd'],
                'lk_u_plus_3_sd'  => $array_lk_u['plus_3_sd'],
                'lk_u_array_anak' => $array_lk_u['array_anak'],
            ])->render();

        return $returnAjax;

    }

}
