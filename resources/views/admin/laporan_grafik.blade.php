@extends('admin.layout')
@section('content')
<script type="text/javascript" src="{{ URL::to('jqplot/jquery.jqplot.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('jqplot/plugins/jqplot.barRenderer.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('jqplot/plugins/jqplot.pieRenderer.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('jqplot/plugins/jqplot.categoryAxisRenderer.js') }} "></script>
<script type="text/javascript" src="{{ URL::to('jqplot/plugins/jqplot.pointLabels.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to('jqplot/jquery.jqplot.min.css') }}" />
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Laporan Grafik</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/admin') }}">Home</a></li>
               <li class="breadcrumb-item active"><a href="{{ URL::to('/admin/laporan-grafik') }}">Laporan Bulanan</a></li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">Laporan Bulanan</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form role="form" method="POST" action="{{ URL::to('/admin/laporan-grafik') }}">
                  {{ csrf_field() }}
                  <div class="card-body">
                     <div class="form-group">
                        <label>Tahun</label>
                        <select class="form-control" name="tahun" id="tahun">
                        @for($i = 2019; $i <= $tahun_sekarang;$i++)
                        <option value="{{ $i }}" {{ ($tahun_sekarang == $i) ? 'selected' : '' }}>{{ $i }}</option>
                        @endfor
                        </select>
                        @if($errors->has('level'))
                        <div class="text-danger">
                           {{ $errors->first('level')}}
                        </div>
                        @endif                    
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
               @if(!empty($_POST))
               <div id="chart2"></div>
               @endif
               <!-- /.card -->
            </div>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript">

   @if(!empty($_POST))

      $(document).ready(function(){
      @php ($s1 = "")
      @php ($s2 = "")
      @php ($bulan = "")
      
      @foreach ($data as $d)
         @php ($s1 .= $d->jml_imunisasi . ',')
         @php ($s2 .= $d->jml_pemeriksaan . ',')
         @php ($bulan .= "'" . $d->nama . "',")
      @endforeach	
      
         var s1 = [{!! substr($s1,0,-1) !!}];
         var s2 = [{!! substr($s2,0,-1) !!}];
         var ticks = [{!!substr($bulan,0,-1) !!}];
         
         plot2 = $.jqplot('chart2', [s1, s2], {
            title:'Jumlah pasien Imunisasi & Pemeriksaan tahun {{ $tahun_sekarang }}',
            series:[
               { label: 'Imunisasi' },
               { label: 'Pemeriksaan' }
            ], 
                  seriesDefaults: {
                        renderer:$.jqplot.BarRenderer,
                        pointLabels: { show: true }
                  },
                  legend: {
                        renderer: jQuery.jqplot.EnhancedLegendRenderer,
                  show: true, 
                  location: 's', 
                  placement: 'outsideGrid',
                  rendererOptions: {
                  numberRows: '1',
                  },
            },
                  axes: {
                        xaxis: {
                           renderer: $.jqplot.CategoryAxisRenderer,
                           ticks: ticks
                        }
                  }
         });
      
         $('#chart2').bind('jqplotDataHighlight', 
            function (ev, seriesIndex, pointIndex, data) {
               $('#info2').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
         );
            
         $('#chart2').bind('jqplotDataUnhighlight', 
            function (ev) {
               $('#info2').html('Nothing');
            }
         );
      });

   @endif

   
</script>
@endsection