@extends('admin.layout')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Laporan bulanan</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/admin') }}">Home</a></li>
               <li class="breadcrumb-item active"><a href="{{ URL::to('/admin/laporan-imunisasi') }}">Laporan Imunisasi</a></li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <ul id="myTabHeader" class="nav nav-pills" style="margin-bottom: 10px;">
               <li class="nav-item">
                  <a href="{{ URL::to('/admin/laporan-imunisasi') }}"  class="nav-link" id="tab_imunisasi">Laporan Imunisasi</a>
               </li>
               <li class="nav-item">
                  <a href="#kesesuaian" class="nav-link active" id="tab_bb_u">Laporan Kesesuaian Jadwal</a>
               </li>
            </ul>
            <div class="tab-content">
               
               
               <div class="tab-pane fade show active" id="kesesuaian">
                  <div class="card card-primary">
                     <div class="card-header">
                        <h3 class="card-title">Laporan Kesesuaian Imunisasi</h3>
                     </div>
                     <!-- /.card-header -->
                     <!-- form start -->
                     <form role="form" method="POST" action="{{ URL::to('/admin/laporan-kesesuaian-imunisasi') }}">
                        {{ csrf_field() }}
                        <div class="card-body">
                           
                           <div class="form-group">
                              <label>Filter</label>
                              <select class="form-control" name="filter">
                                 <!-- <option value="SEMUA" {{ $filter === "SEMUA" ? 'selected' : '' }}>Januari</option> -->
                                 <option value="SESUAI" {{ $filter  === "SESUAI" ? 'selected' : '' }}>Sesuai</option>
                                 <option value="TIDAK-SESUAI" {{ $filter  == "TIDAK-SESUAI" ? 'selected' : '' }}>Tidak Sesuai</option>                              
                              </select>
                           </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                           <button type="submit" name="btnImunisasi" class="btn btn-primary">Submit</button>
                        </div>
                     </form>
                  </div>
                  @if(!empty($_POST))
                  
                     <div class="tab-pane fade show active" id="detail">
                        <table id="dataTablesDetail" class="table table-bordered table-striped">
                           <thead>
                              <tr>
                                 <th>Nama</th>
                                 <th>Usia</th>
                                 <th>Imunisasi</th>                                 
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($data as $o)
                              <tr>
                                 <td>{{ $o['pasien'] }}</td>
                                 <td>{{ $o['usia'] . ' Bulan'}}</td>
                                 <td>{{ $o['imunisasi'] }}</td>                                 
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                     
                  </div>
                  @endif
               </div>



            </div>
         </div>
      </div>
   </div>
</section>

<script>

   $(function () {
     
     $('#dataTablesDetail').DataTable({
       "paging": true,
       "lengthChange": false,
       "searching": true,
       "ordering": true,
       "info": true,
       "autoWidth": false,
       "responsive": true,
     });


   });
</script>

@endsection