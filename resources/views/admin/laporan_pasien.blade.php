@extends('admin.layout')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Laporan bulanan</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/admin') }}">Home</a></li>
               <li class="breadcrumb-item active"><a href="{{ URL::to('/admin/laporan-pasien') }}">Laporan Pasien</a></li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">

            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">Laporan Pasien</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form role="form" method="POST" action="{{ URL::to('/admin/laporan-pasien') }}">
                  {{ csrf_field() }}
                  <div class="card-body">
                     
                     <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status">
                           <option value="semua" {{ $status === 'semua' ? 'selected' : '' }}>Semua</option>
                           <option value="aktif" {{ $status === 'aktif' ? 'selected' : '' }}>Pasien Aktif</option>
                           <option value="tidak-aktif" {{ $status === 'tidak-aktif' ? 'selected' : '' }}>Pasien Tidak Aktif</option>                           
                        </select>
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <button type="submit" name="btnImunisasi" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            
            @if(!empty($_POST))
               <table id="dataTables" class="table table-bordered table-striped">                        
                  <thead>
                     <tr>
                        <th>Nama</th>
                        <th>Orangtua</th>
                        <th>Kunjungan terakhir</th>
                        <th>Status</th>                        
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($data as $o)
                     <tr>
                        <td>{{ $o->nama }}</td>
                        <td>{{ $o->orangtua }}</td>
                        <td>{{ $o->tgl_terakhir . ' (' . $o->date_diff . ' hari yang lalu)' }}</td>
                        <!-- <td>{{ $o->status }}</td>                         -->
                        @if($o->status === 'AKTIF')
                        <td><span class="badge badge-success">AKTIF</span></td>
                        @else
                        <td><span class="badge badge-warning">TIDAK AKTIF</span></td>
                        @endif 
                     </tr>
                     @endforeach
                  </tbody>
               </table>
             @endif
         </div>
      </div>
   </div>
</section>

<script>

   $(function () {
     
     $('#dataTablesDetail').DataTable({
       "paging": true,
       "lengthChange": false,
       "searching": true,
       "ordering": true,
       "info": true,
       "autoWidth": false,
       "responsive": true,
     });

    
   });
</script>

@endsection