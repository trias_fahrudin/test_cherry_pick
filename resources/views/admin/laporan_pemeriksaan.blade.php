@extends('admin.layout')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Laporan Pemeriksaan</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/admin') }}">Home</a></li>
               <li class="breadcrumb-item active"><a href="{{ URL::to('/admin/laporan-pemeriksaan') }}">Laporan Pemeriksaan</a></li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">

            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">Laporan Pemeriksaan</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form role="form" method="POST" action="{{ URL::to('/admin/laporan-pemeriksaan') }}">
                  {{ csrf_field() }}
                  <div class="card-body">
                     <div class="form-group">
                        <label>Tahun</label>
                        <select class="form-control" name="tahun">
                        @for($i = 2019; $i <= $tahun_sekarang;$i++)
                           <option value="{{ $i }}" {{ ($tahun_sekarang == $i) ? 'selected' : '' }}>{{ $i }}</option>
                        @endfor
                        </select>
                        @if($errors->has('level'))
                        <div class="text-danger">
                           {{ $errors->first('level')}}
                        </div>
                        @endif                    
                     </div>
                     <div class="form-group">
                        <label>Bulan</label>
                        <select class="form-control" name="bulan">
                           <option value="1" {{ $bulan_sekarang == 1 ? 'selected' : '' }}>Januari</option>
                           <option value="2" {{ $bulan_sekarang == 2 ? 'selected' : '' }}>Februari</option>
                           <option value="3" {{ $bulan_sekarang == 3 ? 'selected' : '' }}>Maret</option>
                           <option value="4" {{ $bulan_sekarang == 4 ? 'selected' : '' }}>April</option>
                           <option value="5" {{ $bulan_sekarang == 5 ? 'selected' : '' }}>Mei</option>
                           <option value="6" {{ $bulan_sekarang == 6 ? 'selected' : '' }}>Juni</option>
                           <option value="7" {{ $bulan_sekarang == 7 ? 'selected' : '' }}>Juli</option>
                           <option value="8" {{ $bulan_sekarang == 8 ? 'selected' : '' }}>Agustus</option>
                           <option value="9" {{ $bulan_sekarang == 9 ? 'selected' : '' }}>September</option>
                           <option value="10" {{ $bulan_sekarang == 10 ? 'selected' : '' }}>Oktober</option>
                           <option value="11" {{ $bulan_sekarang == 11 ? 'selected' : '' }}>November</option>
                           <option value="12" {{ $bulan_sekarang == 12 ? 'selected' : '' }}>Desember</option>                                    
                        </select>
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            
            @if(!empty($_POST))
            <table id="dataTables" class="table table-bordered table-striped">                        
               <thead>
                   <tr>
                     <th>Nama</th>
                     <th>Tanggal</th>
                     <th>Anamnesa</th>
                     <th>Diagnosa</th>
                     <th>Tindakan</th>                              
                     <th>Resep</th>                              
                   </tr>
               </thead>
               <tbody>
                   @foreach($data as $o)
                   <tr>
                     <td>{{ $o->nama_pasien }}</td>
                     <td>{{ $o->tgl }}</td>
                     <td>{{ $o->anamnesa }}</td>
                     <td>{{ $o->diagnosa }}</td>
                     <td>{{ $o->tindakan }}</td>                              
                     <td>{{ $o->resep }}</td>                              
                   </tr>
                   @endforeach
               </tbody>
             </table>
             @endif
         </div>
      </div>
   </div>
</section>

<script>
   $(function () {
     
     $('#dataTables').DataTable({
       "paging": true,
       "lengthChange": false,
       "searching": true,
       "ordering": true,
       "info": true,
       "autoWidth": false,
       "responsive": true,
     });
   });
</script>

@endsection