<html><head></head>
   <body>
      <p style="text-align: center;margin-bottom: 4px;"><strong>DOKTER NAUFAL BARAS, M.Sc. Sp.A</strong></p>
      <hr>
      <table style="border-collapse: collapse; width: 100%; height: 80px;" border="0">
         <tbody>
            <tr>
               <td>
                  <h5 style="margin-top: 4px; margin-bottom: 4px;"><p style="text-align: center;margin-top: 4px;margin-bottom: 4px;">SPESIALIS ANAK</p></h5>
                  <p style="text-align: center">Jl. Kolonel Sugiono No.22 Telp 085239116000</p>
                  <p style="text-align: center">Ruko Lavender - Kertosari Banyuwangi</p>

               </td>               
            </tr>
         </tbody>
      </table>

      <table style="border-collapse: collapse; width: 100%;" border="1">
         <tbody>
            <tr>
               <td style="width: 100%; text-align: center;">
                  <p>Resep</p>
                  
                  {!! nl2br($resep->resep) !!}
               </td>
            </tr>
         </tbody>
      </table>
      <table style="border-collapse: collapse; width: 100%;" border="1">
         <tbody>
            <tr>
               <td>
                  <p>Tanggal : {{ $resep->tanggal }}</p>                  
                  <p>Nama Pasien : {{ $resep->nama }}</p>                                    
                  <p>Usia : {{ $resep->usia }}&nbsp;Bulan</p>
               </td>
               
               
            </tr>
         </tbody>
      </table>
   
</body></html>