@extends('apoteker.layout')
@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Obat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ URL::to('/apoteker') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ URL::to('/apoteker/obat') }}">Data Pengguna</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Data Obat</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ URL::to('/apoteker/obat-edit/' . $obat->id) }}">
                {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" placeholder="" name="nama" value="{{ old('nama') ?? $obat->nama ?? '' }}">
                     @if($errors->has('nama'))
                          <div class="text-danger">
                              {{ $errors->first('nama')}}
                          </div>
                     @endif
                  </div>
                  
                  
                  <div class="form-group">
                    <label>Level</label>
                    <select class="form-control" name="golongan">
                      <option value="obat-bebas" {{ ((old('golongan') ?? $obat->golongan ?? 'obat-bebas') === 'obat-bebas') ? 'selected' : '' }}>Obat Bebas</option>
                      <option value="obat-bebas-terbatas" {{ ((old('golongan') ?? $obat->golongan ?? 'obat-bebas-terbatas') === 'obat-bebas-terbatas') ? 'selected' : '' }}>Obat Bebas terbatas</option>
                      <option value="obat-keras" {{ ((old('golongan') ?? $obat->golongan ?? 'apoteker') === 'obat-keras') ? 'selected' : '' }}>Obat Keras</option>
                      <option value="obat-narkotika" {{ ((old('golongan') ?? $obat->golongan ?? 'apoteker') === 'obat-narkotika') ? 'selected' : '' }}>Obat Narkotika</option>
                    </select>
                    @if($errors->has('golongan'))
                          <div class="text-danger">
                              {{ $errors->first('golongan')}}
                          </div>
                     @endif                    
                  </div>
                                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
        </div>
      </div>
    </section>
@endsection
