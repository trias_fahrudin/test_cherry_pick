@extends('resepsionis.layout')
@section('content')

<script>
  var base_url = '{{ URL::to('/') . '/' }}';
</script>

<!-- barcode -->
{{-- <script type="text/javascript" src="{{ URL::to('webcodecamjs/js/filereader.js') }}"></script> --}}
<script type="text/javascript" src="{{ URL::to('webcodecamjs/js/qrcodelib.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('webcodecamjs/js/webcodecamjs.js') }}"></script>
{{-- <script type="text/javascript" src="{{ URL::to('webcodecamjs/js/main.js') }}"></script> --}}

<style>
   #barcode {
     width: 320px;
     height:240px;
     margin: 0px auto;
     border: 1px solid red;
   }


</style>
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Data Antrian</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/resepsionis') }}">Home</a></li>
               <li class="breadcrumb-item active">Data Antrian</li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header container-fluid">
                  <div class="row">
                     <div class="col-md-4">
                        <h3 class="card-title">Data Antrian untuk tanggal</h3>
                          <div class="input-group" id="tgl_antrian_div" data-target-input="nearest">
                              <input type="text" class="form-control" data-target="#filter_tgl_antrian" name="filter_tgl_antrian" id="filter_tgl_antrian" value="{{ date('Y-m-d') }}" />
                              <div class="input-group-append" data-target="#filter_tgl_antrian" data-toggle="datetimepicker">
                                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                          </div>
                     </div>
                     <div class="col-md-8">
                        <a href="#!" data-toggle="modal" data-target="#pasienModal" class="btn btn-primary float-right">Tambah Data</a>              
                        <a href="#!" style="margin-right: 5px;" data-toggle="modal" data-target="#pasienDilewati" class="btn btn-danger float-right">Pasien dilewati</a>&nbsp;              
                     </div>
                  </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body">

                  <ul id="myTab" class="nav nav-pills">
                     <li class="nav-item">
                         <a href="#barcode" class="nav-link"  id="tab_barcode">Barcode Reader</a>
                     </li>
                     <li class="nav-item">
                         <a href="#antrian" class="nav-link active" id="tab_antrian">Antrian</a>
                     </li>
                 </ul>
                 <div class="tab-content">
                   <div class="tab-pane fade" id="barcode">
                     <canvas width="320" height="240"></canvas> 
                     <script type="text/javascript">
                        var txt = "innerText" in HTMLElement.prototype ? "innerText" : "textContent";
                         var arg = {
                             resultFunction: function(result) {
                                var aChild = document.createElement('li');
                                var hasil = result.code;
                                var exp = hasil.split(':');  
                                $('#antropometri_pasien_id').val(exp[1]);

                                $.ajax({
                                  url: "{{ URL::to('/resepsionis/get-nama-pasien') }}",
                                  data: {pasien_id : exp[1]},
                                  success: function(data) {
                                    $('#div_label_nama_pasien').show();
                                    $('#label_nama_pasien').html(data);
                                  },
                                  complete: function() {
                                    
                                  }
                                });

                                $('#antropometriModal').modal('show');
                             }
                         };
                         new WebCodeCamJS("canvas").init(arg).play();
                     </script>
                       
                   </div>
                   <div class="tab-pane fade  show active" id="antrian">
                     <table id="dataTables" class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th style='white-space: nowrap;'>Kode</th>                        
                              <th style='white-space: nowrap;'>Nomor</th>                   
                              <th>Jenis</th>
                              <th style='white-space: nowrap;'>Nama Pasien</th>
                              <th style='white-space: nowrap;'  >Tanggal Lahir</th>
                              <th>Nama Orangtua</th>
                              <th>Alamat</th>                           
                              <th>Antropometri</th>                           
                              <th style='white-space: nowrap;'>Opsi</th>
                           </tr>
                        </thead>
                        <tbody id="table-antrian-body">                       
                           @foreach($antrian as $a)
                           <tr>
                              <td>{{ $a->kode }}</td>
                              <td>{{ $a->antrian_ke }}</td>
                              @if($a->jenis === 'pemeriksaan')
                              <td>
                                 <span class="badge badge-success">{{ strtoupper($a->jenis) }}</span>                              
                               </td>
                              @else
                              <td><span class="badge badge-warning">{{ strtoupper($a->jenis) }}</span></td>
                              @endif 
                              
                              <td>{{ $a->nama }}</td>
                              <td>{{ $a->tgl_lahir }}</td>
                              <td>{{ $a->ortu }}</td>
                              <td>{{ $a->alamat }}</td>
                              <td>
                                <a href="#!" onClick="$('#div_label_nama_pasien').hide();$('#antropometri_pasien_id').val({{ $a->pasien_id }})" data-toggle="modal" data-target="#antropometriModal">Ukur</a>
                              </td>
                              <td>
                                @if($a->status === 'antri')
                                <a onclick="return confirm('Apakah anda yakin?');" href="{{ URL::to('/resepsionis/set-diperiksa/' . $a->id) }}" class="btn btn-warning btn-xs">DI TANGANI</a>&nbsp;|&nbsp;
                                <a onclick="return confirm('Apakah anda yakin?');" href="{{ URL::to('/resepsionis/set-lewati/' . $a->id) }}" class="btn btn-danger btn-xs">LEWATI</a>
                                @else
                                <span class="badge badge-warning">DALAM PENANGANAN</span>
                                @endif
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                       
                   </div>                     
                 </div>

                 
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
      </div>
   </div>
</section>

<div class="modal fade" id="pasienModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          Tambah pasien antrian untuk tanggal
          <div class="input-group" id="tgl_antrian_div" data-target-input="nearest">
              <input type="text" class="form-control" data-target="#tgl_antrian" name="tgl_antrian" id="tgl_antrian" value="{{ date('Y-m-d') }}" />
              <div class="input-group-append" data-target="#tgl_antrian" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
          </div>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="users-table" style="width:100%">
            <thead>
               <tr>                        
                   <th>Nama</th>
                   <th>Jenis Kelamin</th>
                   <th>Tempat Lahir</th>
                   <th>Tanggal Lahir</th>  
                   <th>Opsi</th>
               </tr>
            </thead>
          </table>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="pasienDilewati" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data antrian yang dilewati</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="users-table-dilewati" style="width:100%">
            <thead>
               <tr>                        
                   <th>Nama</th>
                   <th>Jenis Kelamin</th>
                   <th>Tempat Lahir</th>
                   <th>Tanggal Lahir</th>  
                  <th>Opsi</th>
               </tr>
            </thead>
          </table>
      </div>
      
    </div>
  </div>
</div>
<script>

   
    $("#myTab a").click(function(e){
        $(this).tab('show');
    });

    $('#tgl_antrian,#filter_tgl_antrian').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    var runWorker = true;

    (function worker() {

    var filter_tgl_antrian = $('#filter_tgl_antrian').val();
    $.ajax({
      url: "{{ URL::to('/resepsionis/antrian-ajax') }}",
      data: {tgl_antrian : filter_tgl_antrian},
      success: function(data) {
        $('#table-antrian-body').html(data.html);
      },
      complete: function() {
        if(runWorker){
          setTimeout(worker, 10000);
        }        
      }
    });
    })();
    
    var table = $('#dataTables').DataTable({
       "paging": true,
       "lengthChange": false,
       "searching": true,
       "ordering": false,
       "info": true,
       "autoWidth": false,
       "responsive": true,
       "scrollX": true
     });     

   table.on( 'search.dt', function () {
      
      if(table.search().length > 0){        
        runWorker = false;

        var exp = table.search().split(':');  
        if(exp.length > 1){
          $('#antropometri_pasien_id').val(exp[1]);

          $.ajax({
            url: "{{ URL::to('/resepsionis/get-nama-pasien') }}",
            data: {pasien_id : exp[1]},
            success: function(data) {
              $('#div_label_nama_pasien').show();
              $('#label_nama_pasien').html(data);
            },
            complete: function() {
              
            }
          });

          $('#antropometriModal').modal('show');
        }

        
      }else{
        //runWorker = true;
        location.reload();
      }

  } );    
      
   

    

   $('#users-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: 'data-pasien-json',
          columns: [
              
              { data: 'nama', name: 'nama' },
              { data: 'jk', name: 'jk' },
              { data: 'tempat_lahir', name: 'tempat_lahir' },
              { data: 'tgl_lahir', name: 'tgl_lahir' },
              { data: 'id', name:'id'},
          ],
          "columnDefs": [
             {
               //<a href="#" class="detail" id="1671070911900008" onclick="show_detail('1671070911900008')">BELA RONALDOE</a>
               

               "render": function(data,type,row){
                 
                 return '<a href="#!" onclick="tambah_data_antrian(' + row['id'] +',\'000\')" class="detail btn btn-success btn-xs">Pemeriksaan</a>&nbsp;|&nbsp;<a href="#!" onclick="tambah_data_antrian(' + row['id'] +',\'111\')" class="detail btn btn-warning btn-xs">Imunisasi</a>';
               },
               "targets" : 4
             },
             {
                "render": function(data,type,row){
                 return row['jk'] == 'L' ? 'Laki-laki' : 'Perempuan';
               },
               "targets" : 1  
             }
           ]
      });



   function tambah_data_antrian(id,tipe){
      var tgl_antrian = $('#tgl_antrian').data("date");
      window.location.href = "{{ URL::to('/resepsionis/tambah-data-antrian') }}" + "/" +  id + "/" + tipe + "/" + tgl_antrian;
      // alert(id + " " + tgl_antrian);
   }

   $('#users-table-dilewati').DataTable({
          processing: true,
          serverSide: true,
          ajax: 'data-pasien-dilewati-json',
          columns: [
              
              { data: 'nama', name: 'nama' },
              { data: 'jk', name: 'jk' },
              { data: 'tempat_lahir', name: 'tempat_lahir' },
              { data: 'tgl_lahir', name: 'tgl_lahir' },
              { data: 'id', name:'id'},
          ],
          "columnDefs": [
             {
               //<a href="#" class="detail" id="1671070911900008" onclick="show_detail('1671070911900008')">BELA RONALDOE</a>
               "render": function(data,type,row){                 
                 return "<a href='{{ URL::to('/resepsionis/masukkan-kembali-data-antrian-dilewati') }}" + "/" +  row['id'] + "' class='detail btn btn-success'>Masukkan antrian</a>";
               },
               "targets" : 4
             },
             {
                "render": function(data,type,row){
                 return row['jk'] == 'L' ? 'Laki-laki' : 'Perempuan';
               },
               "targets" : 1  
             }
           ]
      });
</script>
@endsection